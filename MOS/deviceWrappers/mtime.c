#include "../devices/systick.h"
#include "../kernel/regOps.h"
#include "mtime.h"
#include <stdio.h>

// roughly the length of the string.
#define TIME_STRING_LENGHT 20
char timeStr[TIME_STRING_LENGHT];
char swStr[TIME_STRING_LENGHT];

void updateTimeStr(char * str,volatile unsigned long * ptime)
{
	// calulate the time components from the time ulong.
	int hours = (*ptime%(86400*SUPERTICK_FREQ))/(3600*SUPERTICK_FREQ);
	int minutes = (*ptime%(3600*SUPERTICK_FREQ))/(60*SUPERTICK_FREQ);
	int seconds = (*ptime%(60*SUPERTICK_FREQ))/SUPERTICK_FREQ;
	int tenths = *ptime%SUPERTICK_FREQ;

	// this was causing regular cpu exceptions.
	//sprintf(str,TIME_STRING_LENGHT,"%02i:%02i:%02i.%01i\0",hours,minutes,seconds,tenths);
	// So a manual sprintf was written in this case.

	str[0] = (char)(hours/10+0x30);
	str[1] = (char)(hours%10+0x30);
	str[2] = ':';
	str[3] = (char)(minutes/10+0x30);
	str[4] = (char)(minutes%10+0x30);
	str[5] = ':';
	str[6] = (char)(seconds/10+0x30);
	str[7] = (char)(seconds%10+0x30);
	str[8] = '.';
	str[9] = (char)(tenths+0x30);
	str[10] = (char)(0x20);
	str[11] = (char)(0);

	return;
}

int parseTimeStr(char * str,volatile unsigned long  * ptime)
{
	int hours,minutes,seconds,tenths;
	int parts = sscanf(str,"%02i:%02i:%02i.%01i",&hours,&minutes,&seconds,&tenths);

	if (parts == 4) // if all the comonents were extracted.
	{
		// extract the time from the components
		*ptime = 0;
		*ptime += hours*3600*SUPERTICK_FREQ;
		*ptime += minutes*60*SUPERTICK_FREQ;
		*ptime += seconds*SUPERTICK_FREQ;
		*ptime += tenths*(SUPERTICK_FREQ/10);
		return 1;
	} // otherwise return 0
	return 0;
}

// returns the current time in a string.
char * getTimeStr(void)
{
	unsigned long t = gTime();
	updateTimeStr(timeStr,&t);
	return timeStr;
}

// returns the stop watch as a string.
char * getStopWatchStr(void)
{
	static unsigned long oldSW = 0;
	unsigned long sw =  gSW();
	if (oldSW != sw)
	{
		updateTimeStr((char *)swStr,&sw);
		oldSW = sw;
		return swStr;
	} else return NULL;
}

// resets the stopwatch
void resetStopWatch(void)
{
	sSW(0);
	return;
}

// Sets the current time with a string.
int setTime(char * timeStr)
{
	unsigned long t;
	if (parseTimeStr(timeStr,&t)) {
		sTime(t);
		return 1;
	}

	return 0;
}
