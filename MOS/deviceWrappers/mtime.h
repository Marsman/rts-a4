/*
 * mtime.h
 *
 * This module is an abstraction of the systick module in a m-coretx
 * It allows the current time and a stopwatch to be accessed and set
 * through strings.
 *
 *  Created on: 2014-10-05
 *      Author: Matt Thomson
 */

#ifndef MTIME_H_
#define MTIME_H_

// returns the current time in a string.
char * getTimeStr(void);

// returns the stop watch as a string.
char * getStopWatchStr(void);

// resets the stopwatch
void resetStopWatch(void);

// Sets the current time with a string.
int setTime(char * timeStr);

#endif /* MTIME_H_ */
