/*
 * muart.h
 *
 * This module abstracts the UART0 device on the m-cortex.
 * It allows reading of writing of chars from the uart.
 *
 *
 *  Created on: 2014-10-05
 *      Author: Matt Thomson
 */


#ifndef MUART_H_
#define MUART_H_

#define DISPLAY 				11
#define KEYBOARD				12


// Get the uart initialised
/*
 * returns - 1 if init succeded
 * returns - 0 if init failed
 */
int setupUart(void);

// close the uart
void closeUart(void);

void tryRecv(void);

void trySend(void);

// The ISR to be placed in the vector table for UART0.
void uartIsr(void);

#endif /* MUART_H_ */
