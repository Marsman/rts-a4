/*
 * muart.c
 *
 * This module abstracts the UART0 device on the m-cortex.
 * It allows reading of writing of chars from the uart.
 *
 *  Created on: 2014-10-05
 *      Author: Matt Thomson
 */

#include "../kernel/regOps.h"
#include "../kernel/hmessage.h"
#include "../kernel/kmessage.h"
#include "uart.h"
#include <stdio.h>

// UART0 & PORTA Registers
#define GPIO_PORTA_AFSEL_R	(*((volatile unsigned long *)0x40004420))	// GPIOA Alternate Function Select Register
#define GPIO_PORTA_DEN_R	(*((volatile unsigned long *)0x4000451C))	// GPIOA Digital Enable Register
#define UART0_DR_R      	(*((volatile unsigned long *)0x4000C000))	// UART0 Data Register
#define UART0_FR_R      	(*((volatile unsigned long *)0x4000C018))	// UART0 Flag Register
#define UART0_IBRD_R   		(*((volatile unsigned long *)0x4000C024))	// UART0 Integer Baud-Rate Divisor Register
#define UART0_FBRD_R     	(*((volatile unsigned long *)0x4000C028))	// UART0 Fractional Baud-Rate Divisor Register
#define UART0_LCRH_R    	(*((volatile unsigned long *)0x4000C02C))	// UART0 Line Control Register
#define UART0_CTL_R    		(*((volatile unsigned long *)0x4000C030))	// UART0 Control Register
#define UART0_IFLS_R    	(*((volatile unsigned long *)0x4000C034))	// UART0 Interrupt FIFO Level Select Register
#define UART0_IM_R      	(*((volatile unsigned long *)0x4000C038))	// UART0 Interrupt Mask Register
#define UART0_MIS_R      	(*((volatile unsigned long *)0x4000C040))	// UART0 Masked Interrupt Status Register
#define UART0_ICR_R      	(*((volatile unsigned long *)0x4000C044))	// UART0 Interrupt Clear Register

#define INT_UART0           	5           // UART0 Rx and Tx interrupt index (decimal)
#define UART_FR_TXFF        	0x00000020  // UART Transmit FIFO Full
#define UART_FR_RXFE        	0x00000010  // UART Receive FIFO Empty
#define UART_RX_FIFO_ONE_EIGHT	0x00000038  // UART Receive FIFO Interrupt Level at >= 1/8
#define UART_TX_FIFO_SVN_EIGHT	0x00000007  // UART Transmit FIFO Interrupt Level at <= 7/8
#define UART_LCRH_WLEN_8    	0x00000060  // 8 bit word length
#define UART_LCRH_FEN       	0x00000010  // UART Enable FIFOs
#define UART_CTL_UARTEN     	0x00000301  // UART RX/TX Enable
#define UART_INT_TX         	0x020       // Transmit Interrupt Mask
#define UART_INT_RX         	0x010       // Receive Interrupt Mask
#define UART_INT_RT         	0x040       // Receive Timeout Interrupt Mask
#define UART_CTL_EOT        	0x00000010  // UART End of Transmission Enable
#define EN_RX_PA0           	0x00000001  // Enable Receive Function on PA0
#define EN_TX_PA1           	0x00000002  // Enable Transmit Function on PA1
#define EN_DIG_PA0          	0x00000001  // Enable Digital I/O on PA0
#define EN_DIG_PA1          	0x00000002  // Enable Digital I/O on PA1

// Clock Gating Registers
#define SYSCTL_RCGC1_R		(*((volatile unsigned long *)0x400FE104))
#define SYSCTL_RCGC2_R		(*((volatile unsigned long *)0x400FE108))

#define SYSCTL_RCGC1_UART0  	0x00000001  // UART0 Clock Gating Control
#define SYSCTL_RCGC2_GPIOA  	0x00000001  // Port A Clock Gating Control

// Clock Configuration Register
#define SYSCTRL_RCC_R       	(*((volatile unsigned long *)0x400FE060))

#define CLEAR_USRSYSDIV		0xF83FFFFF	// Clear USRSYSDIV Bits
#define SET_BYPASS		0x00000800	// Set BYPASS Bit

#define NVIC_EN0_R		(*((volatile unsigned long *)0xE000E100))	// Interrupt 0-31 Set Enable Register
#define NVIC_EN1_R		(*((volatile unsigned long *)0xE000E104))	// Interrupt 32-54 Set Enable Register

static void UART0_Init(void)
{
    /* Initialize UART0 */
    SYSCTL_RCGC1_R |= SYSCTL_RCGC1_UART0; 	// Enable Clock Gating for UART0
    SYSCTL_RCGC2_R |= SYSCTL_RCGC2_GPIOA; 	// Enable Clock Gating for PORTA

    UART0_CTL_R &= ~UART_CTL_UARTEN;      	// Disable the UART

    // Setup the BAUD rate
    UART0_IBRD_R = 8;	// IBRD = int(16,000,000 / (16 * 115,200)) = int(8.68055)
    UART0_FBRD_R = 44;	// FBRD = int(0.68055 * 64 + 0.5) = 44.0552


    UART0_LCRH_R = (UART_LCRH_WLEN_8);	// WLEN: 8, no parity, one stop bit, without FIFOs)
    UART0_CTL_R = UART_CTL_UARTEN;        // Enable the UART and End of Transmission Interrupts

    GPIO_PORTA_AFSEL_R |= EN_RX_PA0 | EN_TX_PA1;    	// Enable Receive and Transmit on PA1-0
    GPIO_PORTA_DEN_R |= EN_DIG_PA0 | EN_DIG_PA1;   		// Enable Digital I/O on PA1-0

}

static void IntEnable(unsigned long InterruptIndex)
{
    /* Indicate to CPU which device is to interrupt */
    if(InterruptIndex < 32)
        NVIC_EN0_R |= 1 << InterruptIndex;		// Enable the interrupt in the EN0 Register
    else
        NVIC_EN1_R |= 1 << (InterruptIndex - 32);	// Enable the interrupt in the EN1 Register
}

static void UART0_IntEnable(unsigned long flags)
{
    /* Set specified bits for interrupt */
    UART0_IM_R |= flags;
}

static void SetupPIOSC(void)
{
    /* Set BYPASS, clear USRSYSDIV and SYSDIV */
    SYSCTRL_RCC_R = (SYSCTRL_RCC_R & CLEAR_USRSYSDIV) | SET_BYPASS ;	// Sets clock to PIOSC (= 16 MHz)
}


int setupUart(void) // Get the uart inited
{

	/* Initialize UART */


    // bind on the display port
    kbind(DISPLAY);

    SetupPIOSC();			// Set Clock Frequency to 16MHz (PIOSC)
    UART0_Init();			// Initialize UART0
    IntEnable(INT_UART0);		// Enable UART0 interrupts
    UART0_IntEnable(UART_INT_RX | UART_INT_TX);	// Enable Receive and Transmit interrupts
    IntMasterEnable();		// Enable Master (CPU) Interrupts

	// a char is needed to prime the UART on startup
	UART0_DR_R = 0;
    return 1;
}

void tryRecv(void)
{
    if (UART0_MIS_R & UART_INT_RX)
    {
        // RECV done - clear interrupt and make char available to application
    	UART0_ICR_R |= UART_INT_RX;
    	volatile char data = UART0_DR_R;

    	msg recved;
    	recved.data = (char *)(&data);
    	recved.size = 1;
    	if (hsend(&recved,KEYBOARD) != MSG_SUCCESS)
    	{
    		//BREAKPOINT HERE
    		while(1);
    	}
    }
}

// The uart un
static volatile int sendComplete = 1;
static volatile msg * sendmsg = 0;
static volatile int msgiter = 0;

void trySend(void){

	if ((UART0_MIS_R & UART_INT_TX) || sendComplete) {
	    	volatile char data;
	    	// clear int
	    	UART0_ICR_R |= UART_INT_TX;

	    	if (sendmsg == 0) // if there is no message to send
	    	{
	    		if (hrecv(&sendmsg,DISPLAY) == MSG_SUCCESS) // try to get one
	    		{
	    			msgiter = 0;
	    		}
	    	}

	    	if (sendmsg != 0)
	    	{
	    		data = sendmsg->data[msgiter];
	    		UART0_DR_R = data;

	    		msgiter ++;

	    		if (msgiter >= sendmsg->size)
	    		{
	    			msgiter = 0;
	    			del((msg *)sendmsg);
	    			sendmsg = 0;
	    		}
	    		sendComplete = 0;
	    	} else {
	    		sendComplete = 1;
	    	}
	    }
	    return;
}

void uartIsr(void)
{
	tryRecv();
	trySend();
}

