/*
 * mtime.h
 *
 * This module is an abstraction of the systick module in a m-coretx
 * It allows the current time and a stopwatch to be accessed and set
 * through strings.
 *
 *  Created on: 2014-10-05
 *      Author: Matt Thomson
 */
#ifndef SYSTICK_H_
#define SYSTICK_H_

//#define SYSTICK_FREQ 16777216
#define SYSTICK_FREQ 16000000

#define SUPERTICK_FREQ 10

// Get the tick initialised
void setupTick(void);

unsigned long gTime();
void sTime(unsigned long t);
unsigned long gSW();
void sSW(unsigned long t);

// increments the global time and stopwatch.
// Put this in the interrupt vector table
void systick(void);

#endif /* SYSTICK_H_ */
