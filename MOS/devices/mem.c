/*
 * mem.c
 *
 *	This module will create functionality for allocating and deallocating
 *	memory on the heap.
 *
 *  Created on: 2014-09-16
 *      Author: Matt Thomson
 */

#include <stdio.h>
#include <stdint.h>
#include "mem.h"

#define MEM_LEVELS			5
#define MEM_TOTAL_BLOCKS	248
#define MEM_HEAP_OFFSET 	0x4000 // place the heap in the last 80 kib of sysmem



extern void* _sys_memory;
extern int __SYSMEM_SIZE;

// This allows for the system to init on the first call.
static char is_init = 0;

// The following array stores the binary value of weather a memory block is taken or free.
#define MEM_TAKEN 1
#define MEM_FREE 0
static char taken[MEM_TOTAL_BLOCKS];

static int counts[MEM_LEVELS] 			= {128,64,32,16,8};
static int sizes[MEM_LEVELS] 			= {0x80,0x100,0x200,0x400,0x800};
static int memOffsets[MEM_LEVELS] 		= {0,0x4000,0x8000,0xC000,0x10000};
static int arrayOffsets[MEM_LEVELS] 	= {0,128,192,224,240};

// This function is needed to make sure all the blocks are free on startup.
static void init(void)
{
	int i;
	for (i =0; i < MEM_TOTAL_BLOCKS; i++)
		taken[i] = MEM_FREE;
	is_init = 1;
}

/** This function will allocate a block of memory of size
* 	size - The size in bytes of the requested memory block.
* 	returns - Null if invalid size or there is no memory block large enough available.
* 	returns - a pointer to the memory block if success.
**/
char * allocate(int size)
{
	int i, k; // iterators

	if (!is_init) // runs once
		init();

	if (size > 0)
	{
		for (i = 0; i < MEM_LEVELS; i++) // Iterate of all the memory levels from the bottom up
		{
			if (size <= sizes[i]) // If the size fits in the level
			{
				for (k = 0; k < counts[i]; k++) // Search through blocks in the level
				{
					if (taken[arrayOffsets[i]+k] == MEM_FREE)
					{
						// set the taken array
						taken[arrayOffsets[i]+k] = MEM_TAKEN;
						// calculate the return address
						uint32_t block = (uint32_t)(&_sys_memory);
						block += MEM_HEAP_OFFSET;
						block += memOffsets[i];
						block += (sizes[i]*k);
						return (char *)block;
					} // If the block is not free then keep on searching
				} // If none of the blocks in the level are free then move up to the next level
			} // If the size is larger than the level's size then move up to the next level
		}
	}
	return NULL; // If there are none available then return null.
}

/*	This function will deallocte a block of memory that was prevoiusly allocated.
 * 	block - A pointer that is within the block of memory to be freed.
 * 	returns - 1 if the memory was freed successfully
 * 	returns - 0 if the block pointer was invalid.
 * 	returns - 0 if the block pointer is pointing to an already fee pointer.
 */
int deallocate(void * block)
{
	int i; // iterator

	// remove the sysmemory offset from the address
	uint32_t blockOffset = (uint32_t)block - (uint32_t)(&_sys_memory);

	// remove the HEAP offset from the address
	blockOffset -= MEM_HEAP_OFFSET;

	for (i = 0; i < MEM_LEVELS; i++) // iterate over the levels
	{
		// if the block offset is within a level's memory space.
		if ((blockOffset >= memOffsets[i])&&(blockOffset < (memOffsets[i] + sizes[i] * counts[i])))
		{
			// calculate what block is being pointed to.
			int blockIndx = (blockOffset - memOffsets[i])/sizes[i];

			if (taken[arrayOffsets[i]+blockIndx] == MEM_TAKEN) // if the block is taken
			{
				// free the memory block
				taken[arrayOffsets[i]+blockIndx] = MEM_FREE;
				return 1;
			} else {
				return 0; // error - tried to deallocate something that is free
			}
		}
	}
	return 0;
}
