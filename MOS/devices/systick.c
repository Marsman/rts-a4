#include "systick.h"
#include "../kernel/regOps.h"
#include <stdio.h>


// 32 bit at a frequency of 10 hz gives a ~13 year rollover time.
static volatile unsigned long time;
static volatile unsigned long stopwatch;


// SysTick NVIC Registers

#define NVIC_ST_CTRL_R   (*((volatile unsigned long *)0xE000E010))  // SysTick Control and Status Register (STCTRL)
#define NVIC_ST_RELOAD_R (*((volatile unsigned long *)0xE000E014))  // Systick Reload Value Register (STRELOAD)

// SysTick defines

#define NVIC_ST_CTRL_COUNT      0x00010000  // Count Flag for STCTRL
#define NVIC_ST_CTRL_CLK_SRC    0x00000004  // Clock Source for STCTRL
#define NVIC_ST_CTRL_INTEN      0x00000002  // Interrupt Enable for STCTRL
#define NVIC_ST_CTRL_ENABLE     0x00000001  // Enable for STCTRL

void SysTickStart(void)
{
// Set the clock source to internal and enable the counter
NVIC_ST_CTRL_R |= NVIC_ST_CTRL_CLK_SRC | NVIC_ST_CTRL_ENABLE;
}

void SysTickStop(void)
{
// Clear the enable bit to stop the counter
NVIC_ST_CTRL_R &= ~(NVIC_ST_CTRL_ENABLE);
}

void SysTickPeriod(unsigned long Period)
{
// Must be between 0 and 16777216
NVIC_ST_RELOAD_R = Period - 1;
}

void SysTickIntEnable(void)
{
// Set the interrupt bit in STCTRL
NVIC_ST_CTRL_R |= NVIC_ST_CTRL_INTEN;
}

void SysTickIntDisable(void)
{
// Clear the interrupt bit in STCTRL
NVIC_ST_CTRL_R &= ~(NVIC_ST_CTRL_INTEN);
}

void setupTick(void) // Get the tick init
{
	time = 0;
	stopwatch = 0;

	unsigned long Period = SYSTICK_FREQ / SUPERTICK_FREQ;

	SysTickPeriod(Period);
	SysTickIntEnable();
	SysTickStart();
	IntMasterEnable();
}

unsigned long gTime() {
	IntMasterDisable();
	unsigned long t = time;
	IntMasterEnable();
	return t;
}
void sTime(unsigned long t) {
	IntMasterDisable();
	time = t;
	IntMasterEnable();
}

unsigned long gSW() {
	IntMasterDisable();
	unsigned long s = stopwatch;
	IntMasterEnable();
return s;
}

void sSW(unsigned long t) {
	IntMasterDisable();
	stopwatch = t;
	IntMasterEnable();
}

void systick(void)
{
	time ++;
	stopwatch ++;
}
