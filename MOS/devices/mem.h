/*
 * mem.h
 * 	This module will create functionality for allocating and deallocating
 *	memory on the heap.
 *
 *  Created on: 2014-09-16
 *      Author: Matt Thomson
 */

#ifndef MMEM_H_
#define MMEM_H_

/** This function will allocate a block of memory of size
* 	size - The size in bytes of the requested memory block.
* 	returns - Null if invalid size or there is no memory block large enough available.
* 	returns - a pointer to the memory block if success.
**/
char * allocate(int size);

/*	This function will deallocte a block of memory that was prevoiusly allocated.
 * 	block - A pointer that is within the block of memory to be freed.
 * 	returns - 1 if the memory was freed successfully
 * 	returns - 0 if the block pointer was invalid.
 * 	returns - 0 if the block pointer is pointing to an already fee pointer.
 */
int deallocate(void * block);

#endif /* MMEM_H_ */
