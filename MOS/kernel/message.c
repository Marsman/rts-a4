/*
 * message.c
 *
 *  Created on: Nov 14, 2014
 *      Author: matt
 */
#include "kernel.h"
#include "system.h"
#include "regOps.h"
#include "message.h"
#include "../devices/mem.h"

// Only receive side binds. Send side can only send to recvPorts.
msgReturnCode bind(unsigned int recvPort)
{
	volatile struct kcallargs args; // on the stack due to volatile
	args.code = BIND;
	args.arg1 = recvPort;
	assignR7((unsigned long)(&args));
	SVC();   // R7 now points to the arguments.
	return args.rtnvalue;
}


msgReturnCode unbind(unsigned int recvPort)
{
	volatile struct kcallargs args; // on the stack due to volatile
	args.code = UNBIND;
	args.arg1 = recvPort;
	assignR7((unsigned long)(&args));
	SVC();   // R7 now points to the arguments.
	return args.rtnvalue;
}

msgReturnCode send(msg * sendMsg,unsigned int destPort)
{
	volatile struct kcallargs args; // on the stack due to volatile
	args.code = SEND;
	args.arg1 = (unsigned int)sendMsg;
	args.arg2 = destPort;
	assignR7((unsigned long)(&args));
	SVC();   // R7 now points to the arguments.
	return args.rtnvalue;
}
msgReturnCode recv(msg ** recvMsg, unsigned int recvPort)
{
	volatile struct kcallargs args; // on the stack due to volatile
	args.code = RECV;
	args.arg1 = (unsigned int)recvMsg;
	args.arg2 = recvPort;
	assignR7((unsigned long)(&args));
	SVC();   // R7 now points to the arguments.
	return args.rtnvalue;
}

// This will poll on a list of ports. The process will block until this port has activity. zmq_poll

msgReturnCode poll(unsigned int * pollList, int ListSize)
{
	volatile struct kcallargs args; // on the stack due to volatile
	args.code = POLL;
	args.arg1 = (unsigned int)pollList;
	args.arg2 = ListSize;
	assignR7((unsigned long)(&args));
	SVC();   // R7 now points to the arguments.
	return args.rtnvalue;
}

// Deletes a message that you received from receive.
msgReturnCode del(msg * delmsg){
	if (deallocate(delmsg))
		return MSG_SUCCESS;
	else
		return MSG_FAIL;
}

