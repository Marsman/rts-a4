
/*

* message.h

*

* Created on: Nov 13, 2014

* Author: matt

*/

#ifndef MESSAGE_H_
#define MESSAGE_H_

typedef struct {
	unsigned int size;
	char * data;
} msg;

typedef int msgReturnCode;

enum {MSG_SUCCESS, MSG_FAIL,INVALID_PORT, INVALID_MSG_SIZE};

// Only receive side binds. Send side can only send to recvPorts.
msgReturnCode bind(unsigned int recvPort);

msgReturnCode send(msg * sendMsg,unsigned int destPort);

msgReturnCode recv(msg ** recvMsg, unsigned int recvPort);

// This will poll on a list of ports. The process will block until this port has activity. zmq_poll

msgReturnCode poll(unsigned int * pollList, int ListSize);

msgReturnCode unbind(unsigned int recvPort);

msgReturnCode del(msg * delmsg);

#endif /* MESSAGE_H_ */
