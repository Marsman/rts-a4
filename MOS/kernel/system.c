/*
 * system.c
 *
 *	The following module is a user interface for this real time operating system.
 *
 *  Created on: Oct 30, 2014
 *      Author: matt
 */

#include "kernel.h"
#include "system.h"
#include "regOps.h"

// Change the priority of the current process.
void nice(unsigned int newPriority)
{
	volatile struct kcallargs args; // on the stack due to volatile
	args.code = NICE;
	args.arg1 = newPriority;
	assignR7((unsigned long)(&args));
	SVC();   // R7 now points to the arguments.
}

// Do not call this directly. This is called automatically at the end of the process.
void terminate(void)
{
    volatile struct kcallargs args; // on the stack due to volatile
    args.code = TERMINATE;
    assignR7((unsigned long)(&args));
    SVC();   // R7 now points to the arguments.
}

// Returns the PID of the current process.
unsigned int getID(void)
{
	volatile struct kcallargs args; // on the stack due to volatile
	args.code = GETID;
	assignR7((unsigned long)(&args));
	SVC();   // R7 now points to the arguments.

	// might need a small wait here if args.rtnvalue does not change in time.
	return args.rtnvalue;
}

// The starting point for the OS.
int main(void) {
	// code start point.
	SVC(); // should never come back from this.
	while (1) { }
	return 0;
}
