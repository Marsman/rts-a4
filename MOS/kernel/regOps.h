/*
 * regOps.h
 *
 *  Created on: Oct 30, 2014
 *      Author: matt
 */

#ifndef REGOPS_H_
#define REGOPS_H_

unsigned long get_PSP(void);
unsigned long get_MSP(void);
void set_PSP(volatile unsigned long ProcessStack);
void set_MSP(volatile unsigned long MainStack);
void save_registers();
void restore_registers();
unsigned long get_SP();
void assignR7(volatile unsigned long data);
void IntMasterEnable(void);
void IntMasterDisable(void);
#endif /* REGOPS_H_ */
