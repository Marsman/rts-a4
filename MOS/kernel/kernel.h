/*
 * mkernel.h
 *
 *  Created on: Oct 22, 2014
 *      Author: matt
 */

#ifndef MKERNEL_H_
#define MKERNEL_H_

#include "kprocess.h"

// allow overriding the stacksize for the processes.
#ifndef STACK_SIZE
#define STACK_SIZE 1000 // chose 1000 to allow for PCB to fit alongside stack.
#endif

#define SVC() __asm(" SVC #0")

enum kernelcallcodes {GETID, NICE, TERMINATE,BIND,UNBIND,SEND,RECV,POLL};

struct kcallargs
{
unsigned int code;
unsigned int rtnvalue;
unsigned int arg1;
unsigned int arg2;
};

#define NUM_PRIORITIES 5

void SVCall(void); // Call SVC()  to access

// increments the global time and stopwatch.
// Put this in the interrupt vector table
void systickIsr(void);

void getRunningPir(struct pcb *** running,unsigned int ** currentPriority);

#endif /* MKERNEL_H_ */
