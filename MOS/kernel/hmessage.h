/*
 * hmessage.h
 *
 *  Created on: Nov 25, 2014
 *      Author: matt
 */

#ifndef HMESSAGE_H_
#define HMESSAGE_H_

#include "message.h"

msgReturnCode hsend(msg * sendMsg,unsigned int destPort);

msgReturnCode hrecv(msg ** recvMsg, unsigned int recvPort);

#endif /* HMESSAGE_H_ */
