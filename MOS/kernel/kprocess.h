/*
 * kprocess.h
 *
 *  Created on: Nov 16, 2014
 *      Author: matt
 */

#include "pinfo.h"

#ifndef KPROCESS_H_
#define KPROCESS_H_


/* Cortex default stack frame */
struct stack_frame
{
/* Registers saved by s/w (explicit) */
/* There is no actual need to reserve space for R4-R11, other than
 * for initialization purposes.  Note that r0 is the h/w top-of-stack.
 */
unsigned long r4;
unsigned long r5;
unsigned long r6;
unsigned long r7;
unsigned long r8;
unsigned long r9;
unsigned long r10;
unsigned long r11;
/* Stacked by hardware (implicit)*/
unsigned long r0;
unsigned long r1;
unsigned long r2;
unsigned long r3;
unsigned long r12;
unsigned long lr;	/* Address of process-terminate function */
unsigned long pc;  	/* Address of process entry point */
unsigned long xpsr;	/* Initialize to 0x010000000 */
};

/* Process control block */

struct pcb
{
/* Stack pointer - r13 (PSP) */
unsigned long sp;
unsigned int pid;
/* Links to adjacent PCBs */
struct pcb *next;
struct pcb *prev;
};


// add a process to the system. Only to be called at startup.
int reg_proc(struct pcb ** running,unsigned int * currentPriority,struct processInfo *pinfo);

void kterminate(struct pcb ** running,unsigned int * currentPriority);

void knice(unsigned int newPriority, struct pcb ** running,unsigned int * currentPriority);

void kcontextSwitch(struct pcb ** running,unsigned int * currentPriority);

// kernel get PID
unsigned int kgetID(struct pcb ** running,unsigned int * currentPriority);

#endif /* KPROCESS_H_ */
