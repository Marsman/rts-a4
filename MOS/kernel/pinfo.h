/*
 * pinfo.h
 *
 *  Created on: Nov 17, 2014
 *      Author: matt
 */

#ifndef PINFO_H_
#define PINFO_H_

struct processInfo
{
	unsigned int pid;
	unsigned int priority;
	void (*pfunc)();
};

#endif /* PINFO_H_ */
