/*
 * system.h
 *
 *The following module is a user interface for this real time operating system.
 *
 *  Created on: Oct 30, 2014
 *      Author: matt
 */

#ifndef SYSTEM_H_
#define SYSTEM_H_

// Change the priority of the current process.
void nice(unsigned int newPriority);

// Do not call this directly. This is called automatically at the end of the process.
void terminate(void);

// Returns the PID of the current process.
unsigned int getID(void);

#endif /* SYSTEM_H_ */
