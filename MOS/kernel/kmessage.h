/*
 * kmessage.h
 *
 *  Created on: Nov 14, 2014
 *      Author: matt
 */

#include "message.h"
#include "kprocess.h"

#ifndef KMESSAGE_H_
#define KMESSAGE_H_

#define NUM_PORTS 32
#define MAX_MSGS 32


void initMsg(void);

// Only receive side binds. Send side can only send to recvPorts.
msgReturnCode kbind(unsigned int recvPort);

msgReturnCode kunbind(unsigned int recvPort);

// you give it a pointer to a message structure. It does the rest.
msgReturnCode ksend(msg * sendMsg,unsigned int destPort,struct pcb ** running,unsigned int * currentPriority);

// you give it a pointer to a message pointer. It populates this pointer
// This effectively allocates the message for your use.
msgReturnCode krecv(msg ** recvMsg, unsigned int recvPort,struct pcb ** running,unsigned int * currentPriority);

msgReturnCode kpoll(unsigned int * pollList,unsigned int listsize,struct pcb ** running,unsigned int * currentPriority);


#endif /* KMESSAGE_H_ */
