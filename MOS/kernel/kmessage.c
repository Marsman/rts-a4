/*
 * kmessage.c
 *
 *  Created on: Nov 14, 2014
 *      Author: matt
 */

#include "kmessage.h"
#include "../devices/mem.h"
#include "../devices/uart.h"
#include "string.h"
#include "../util/pqueue.h"
#include "kprocess.h"
#include "kernel.h"
#include "regOps.h"


pqueue ports[NUM_PORTS];

typedef struct {
	struct pcb * PCB;
	void * next;
	unsigned int oldPriority;
	char pollingList[NUM_PORTS];
} pollingProc;

pollingProc * pollingProcs;

void initMsg(void)
{
	pollingProcs = 0;
	int i;
	for (i = 0; i < NUM_PORTS; i ++)
		ports[i].circbuff = 0;
}

static void wakePollingProcs(unsigned int recvPort,struct pcb ** running,unsigned int * currentPriority)
{
	pollingProc * itr = pollingProcs;
	pollingProc * lastitr = 0;

	if (itr != 0)
	{
		running[*currentPriority]->sp = get_PSP();

		while (itr != 0)
		{
			if (itr->pollingList[recvPort] == 1) // if this process is polling on this port.
			{
				struct pcb * thisProcess = itr->PCB;
				unsigned int newPriority = itr->oldPriority;

				//********* Place
				if (running[newPriority] == 0)
				{
					running[newPriority] = thisProcess;
					thisProcess->next = thisProcess;
					thisProcess->prev = thisProcess;
				} else {
					// reassign links on either side
					struct pcb * newPrev = running[newPriority]->prev;
					struct pcb * newNext = running[newPriority];
					newPrev->next = thisProcess;
					newNext->prev = thisProcess;
					thisProcess->next = newNext;
					thisProcess->prev = newPrev;
					// place the new block as running
					running[newPriority] = thisProcess;
				}
				*currentPriority = newPriority;

				// ********** remove from pollingprocs and clean up.
				if (lastitr == 0)
				{
					pollingProcs = itr->next;

					deallocate(itr);

					itr = pollingProcs;

					lastitr = 0;

				} else {
					lastitr->next = itr->next;

					deallocate(itr);

					itr = lastitr->next;

				}
			} else {
				lastitr = itr;
				itr = itr->next;
			}
		}

		//********* Search for the next logical process to execute from.
		int indexPriority;
		for (indexPriority = NUM_PRIORITIES - 1; indexPriority >= 0; indexPriority--)
		{
			// set the new currentPriority
			if (running[indexPriority] != 0)
			{
				*currentPriority = indexPriority;
				break;
			}
		}
		set_PSP(running[*currentPriority]->sp);
	}
}

// Only receive side binds. Send side can only send to recvPorts.
msgReturnCode kbind(unsigned int recvPort)
{
	// is this a valid port number?
	if (recvPort >= NUM_PORTS)
		return INVALID_PORT;

	// is this port taken?
	if (ports[recvPort].circbuff !=0)
		return INVALID_PORT;

	// init the queue.
	if (!initpQueue(&ports[recvPort],MAX_MSGS))
		return MSG_FAIL;

	return MSG_SUCCESS;
}

msgReturnCode kunbind(unsigned int recvPort)
{
	// is this a valid port number?
	if (recvPort >= NUM_PORTS)
		return INVALID_PORT;

	// is this port taken?
	if (ports[recvPort].circbuff == 0)
		return INVALID_PORT;

	if (!deletepQueue(&ports[recvPort]))
		return MSG_FAIL;

	ports[recvPort].circbuff = 0;
	return MSG_SUCCESS;
}


msgReturnCode ksend(msg * sendMsg,unsigned int destPort,struct pcb ** running,unsigned int * currentPriority)
{
	// is this a valid port number?
	if (destPort >= NUM_PORTS)
		return INVALID_PORT;

	// make sure someone owns this port.
	if (ports[destPort].circbuff ==0)
		return INVALID_PORT;

	int msize = sendMsg->size + sizeof(msg);

	char * newStorage = allocate(msize);

	if (newStorage == 0)
		return INVALID_MSG_SIZE;


	// copy over the msg struct
	memcpy(newStorage,sendMsg,sizeof(msg));

	// copy over the msg data
	memcpy(newStorage+sizeof(msg),sendMsg->data,sendMsg->size);

	// relink the data
	msg * newMsg = (msg * )newStorage;
	newMsg->data = newStorage + sizeof(msg);


	if (!enpQueue(newStorage,&ports[destPort]))
	{
		deallocate(newStorage);
		return MSG_FAIL;
	}



	wakePollingProcs(destPort,running,currentPriority);

	// also wake the uart
	if (destPort == DISPLAY)
		trySend();

	return MSG_SUCCESS;
}

msgReturnCode krecv(msg ** recvMsg, unsigned int recvPort,struct pcb ** running,unsigned int * currentPriority)
{
	// is this a valid port number?
	if (recvPort >= NUM_PORTS)
		return INVALID_PORT;

	// make sure someone owns this port.
	if (ports[recvPort].circbuff ==0)
		return INVALID_PORT;

	if (!depQueue((void **)recvMsg,&ports[recvPort]))
		return MSG_FAIL;

	wakePollingProcs(recvPort,running,currentPriority);

	return MSG_SUCCESS;
}

msgReturnCode kpoll(unsigned int * pollList,unsigned int listsize,struct pcb ** running,unsigned int * currentPriority)
{
	pollingProc * newProc = (pollingProc *)allocate(sizeof(pollingProc));

	if (newProc == 0)
		return MSG_FAIL;

	newProc->PCB = running[*currentPriority];
	newProc->oldPriority = *currentPriority;

	if (pollingProcs == 0)
	{
		pollingProcs = newProc;
		newProc->next = 0;
	} else {
		pollingProc * old = pollingProcs;
		newProc->next = old;
		pollingProcs = newProc;
	}

	int i;

	for (i = 0; i < NUM_PORTS; i++)
	{
		newProc->pollingList[i] = 0;
	}

	for (i = 0; i < listsize; i++)
	{
		if (pollList[i] < NUM_PORTS)
			newProc->pollingList[pollList[i]] = 1;
	}

	struct pcb * thisProcess = newProc->PCB;
	thisProcess->sp = get_PSP();
	//********* Cleanup
	if (thisProcess->next == thisProcess)
	{
		running[*currentPriority] = 0; // empty out the linked list
	} else { // snip thisProcess from the list.
		thisProcess->prev->next = thisProcess->next;
		thisProcess->next->prev = thisProcess->prev;
		running[*currentPriority] = thisProcess->next;
	}

	//********* Search for the next logical process to execute from.
	int indexPriority;
	for (indexPriority = NUM_PRIORITIES - 1; indexPriority >= 0; indexPriority--)
	{
		// set the new currentPriority
		if (running[indexPriority] != 0)
		{
			*currentPriority = indexPriority;
			break;
		}
	}

	set_PSP(running[*currentPriority]->sp);

	return MSG_SUCCESS;
}
