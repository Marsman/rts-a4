/*
 * hmessage.c
 *
 *  Created on: Nov 25, 2014
 *      Author: matt
 */


#include "kernel.h"
#include "system.h"
#include "regOps.h"
#include "message.h"
#include "hmessage.h"
#include "kmessage.h"
#include "../devices/mem.h"

msgReturnCode hsend(msg * sendMsg,unsigned int destPort)
{
	IntMasterDisable();
	save_registers();
	struct pcb ** runn;
	unsigned int * currpri;
	getRunningPir(&runn,&currpri);

	msgReturnCode rtn = ksend(sendMsg,destPort,runn,currpri);

	restore_registers();
	IntMasterEnable();
	return rtn;
}
msgReturnCode hrecv(msg ** recvMsg, unsigned int recvPort)
{

	IntMasterDisable();
	save_registers();
	struct pcb ** runn;
	unsigned int * currpri;
	getRunningPir(&runn,&currpri);

	msgReturnCode rtn =  krecv(recvMsg,recvPort,runn,currpri);

	restore_registers();
	IntMasterEnable();
	return rtn;
}

