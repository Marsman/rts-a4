/*
 * kprocess.c
 *
 *  Created on: Nov 16, 2014
 *      Author: matt
 */

#include "regOps.h"
#include "kernel.h"
#include "kprocess.h"
#include "../devices/mem.h"
#include "system.h"


// add a process to the system. Only to be called at startup.
int reg_proc(struct pcb ** running,unsigned int * currentPriority,struct processInfo *pinfo)
{
	/* allocated mem:
	 * |	PCB (sizeof pcb)
	 * |	STACK (stacksize)
	 * |	STACK
	 * |	STACK <- SP
	 *
	 * if a process stack overflows it writes over its own pcb. Justice served.
	 */

	// allocate the required memory for a process in sysmem.
	struct pcb * newPCB = (struct pcb * )allocate(sizeof(struct pcb) + STACK_SIZE);
	if (newPCB == 0)
	{
		//printf("Debug Error: Could not allocate stack for pid: %i",pinfo->pid);
		return 0;
	}

	// stack pointer is init'ed to the bottom of the new memory space
	unsigned long sp = (unsigned long)((void *)newPCB)+sizeof(struct pcb)+STACK_SIZE;
	newPCB->sp = sp;
	newPCB->pid = pinfo->pid;

	// link the new pcb into it's prioritie's list. Special case if its the first entry in a list.
	if (running[pinfo->priority] == 0) // first block in circular list.
	{
		running[pinfo->priority] = newPCB;
		running[pinfo->priority]->next = running[pinfo->priority];
		running[pinfo->priority]->prev = running[pinfo->priority];
	} else { // inject the new process before running.
		newPCB->prev = running[pinfo->priority]->prev;
		newPCB->next = running[pinfo->priority];

		running[pinfo->priority]->prev->next = newPCB;
		running[pinfo->priority]->prev = newPCB;
	}

	// subtract the size of the sf
	struct stack_frame * sf = (struct stack_frame *)(sp - sizeof(struct stack_frame));

	sf->lr = (unsigned long)terminate;
	sf->pc = (unsigned long)pinfo->pfunc;
	sf->xpsr = 0x01000000;
	// load with values that are traceable
	sf->r0 = 0x00000000;
	sf->r1 = 0x11111111;
	sf->r2 = 0x22222222;
	sf->r3 = 0x33333333;
	sf->r4 = 0x44444444;
	sf->r5 = 0x55555555;
	sf->r6 = 0x66666666;
	sf->r7 = 0x77777777;
	sf->r8 = 0x88888888;
	sf->r9 = 0x99999999;
	sf->r10 = 0xaaaaaaaa;
	sf->r11 = 0xbbbbbbbb;
	sf->r12 = 0xcccccccc;

	// start the sp pointing at r4
	newPCB->sp = (unsigned long)(&sf->r4);

	// if this is the highest priority process then make it the starting process.
	if (pinfo->priority > *currentPriority)
		*currentPriority = pinfo->priority;

	// success
	return 1;
}

void kterminate(struct pcb ** running,unsigned int * currentPriority)
{
	int newPriority;
	unsigned int currpri = *currentPriority;
	struct pcb * theRunningProcess = running[currpri];

	if (running[currpri]->next == running[currpri])
	{ // if this is the last process on this queue's priority.
		// blast away the running process.
		running[currpri] = 0;

		// iterate over all lower priorities
		for (newPriority = currpri - 1; newPriority >= 0; newPriority--)
		{
			// set the new currentPriority
			if (running[newPriority] != 0)
			{
				*currentPriority = newPriority;
				currpri = *currentPriority;
				break;
			}

		}
	} else { // there are others at this priority so we need to remove running from the linked list.

		struct pcb * nextrunning = running[currpri]->next;

		// snip running from the linked list.
		nextrunning->prev = running[currpri]->prev;
		running[currpri]->prev->next = nextrunning;

		running[currpri] = nextrunning;
	}

	deallocate(theRunningProcess);
	//if (!deallocate(theRunningProcess))
		//printf("\nError deallocating terminated process.");


	// load the new stack pointer and begin execution on the new process.
	set_PSP(running[currpri]->sp);
	restore_registers();
	__asm("	movw 	LR,#0xFFFD");  /* Lower 16 [and clear top 16] */
	__asm("	movt 	LR,#0xFFFF");  /* Upper 16 only */
	__asm("	bx 	LR");          /* Force return to PSP */
}

// kernel change the priority
void knice(unsigned int newPriority,struct pcb ** running,unsigned int * currentPriority)
{
	unsigned int currPri = * currentPriority;
	if (newPriority < NUM_PRIORITIES && newPriority != currPri)
	{

		struct pcb * thisProcess = running[currPri];

		thisProcess->sp = get_PSP();

		//********* Cleanup
		if (thisProcess->next == thisProcess)
		{
			running[currPri] = 0; // empty out the linked list
		} else { // snip thisProcess from the list.
			thisProcess->prev->next = thisProcess->next;
			thisProcess->next->prev = thisProcess->prev;
			running[currPri] = thisProcess->next;
		}

		//********* Place
		if (running[newPriority] == 0)
		{
			running[newPriority] = thisProcess;
			thisProcess->next = thisProcess;
			thisProcess->prev = thisProcess;
		} else {
			// reassign links on either side
			struct pcb * newPrev = running[newPriority]->prev;
			struct pcb * newNext = running[newPriority];
			newPrev->next = thisProcess;
			newNext->prev = thisProcess;
			thisProcess->next = newNext;
			thisProcess->prev = newPrev;
			// place the new block as running
			running[newPriority] = thisProcess;
		}
		*currentPriority = newPriority;
		currPri = * currentPriority;
	}

	//********* Search for the next logical process to execute from.
	int indexPriority;
	for (indexPriority = NUM_PRIORITIES - 1; indexPriority >= 0; indexPriority--)
	{
		// set the new currentPriority
		if (running[indexPriority] != 0)
		{
			*currentPriority = indexPriority;
			currPri = * currentPriority;
			break;
		}
	}

	set_PSP(running[currPri]->sp);
}


void kcontextSwitch(struct pcb ** running,unsigned int * currentPriority)
{
	unsigned int currPri = * currentPriority;
	IntMasterDisable();
	// store state
	save_registers();
	running[currPri]->sp = get_PSP();

	// find next process

	running[currPri] = running[currPri]->next;

	// load next state
	set_PSP(running[currPri]->sp);
	restore_registers();
	IntMasterEnable();

}

// kernel get PID
unsigned int kgetID(struct pcb ** running,unsigned int * currentPriority)
{
	return running[*currentPriority]->pid;
}
