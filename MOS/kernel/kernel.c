/*
 * mkernel.c
 *
 * The following module represents the inner workings of the operating system
 *
 *  Created on: Oct 22, 2014
 *      Author: matt
 */

#include "../devices/uart.h"
#include "../devices/systick.h"
#include "../apps/apps.h"
#include "kernel.h"
#include "regOps.h"

#include "kmessage.h"
#include "kprocess.h"

#define NUM_PRIORITIES 5
struct pcb * running[NUM_PRIORITIES] = {0,0,0,0,0};
unsigned int currentPriority = 0;

void getRunningPir(struct pcb *** prunning,unsigned int ** pcurrentPriority)
{
	*prunning = running;
	*pcurrentPriority = &currentPriority;
}

// called by SVC terminate
void SVCall(void)
{
	/* Save LR for return via MSP or PSP */
	__asm(" 	PUSH 	{LR}");

	/* Trapping source: MSP or PSP? */
	__asm(" 	TST 	LR,#4");	/* 3rd bit indicates MSP (0) or PSP (1) */
	__asm(" 	BNE 	RtnViaPSP");

	/* Trapping source is MSP - save r4-r11 on stack (default, so just push) */
	__asm(" 	PUSH 	{r4-r11}");
	__asm(" 	MRS	r0,msp");
	__asm(" 	BL	SVCHandler");	/* r0 is MSP */
	__asm(" 	POP	{r4-r11}");
	__asm(" 	POP 	{PC}");

	/* Trapping source is PSP - save r4-r11 on psp stack (MSP is active stack) */
	__asm("RtnViaPSP:");
	__asm(" 	mrs 	r0,psp");
	__asm("		stmdb 	r0!,{r4-r11}");	/* Store multiple, decrement before */
	__asm("		msr	psp,r0");
	__asm(" 	BL	SVCHandler");	/* r0 Is PSP */

	/* Restore r4..r11 from trapping process stack  */
	__asm(" 	mrs 	r0,psp");
	__asm("		ldmia 	r0!,{r4-r11}");	/* Load multiple, increment after */
	__asm("		msr	psp,r0");
	__asm(" 	POP 	{PC}");

}

void SVCHandler(struct stack_frame *argptr) //argptr is psp or msp first argument
{
	static int firstSVCcall = 1;
	struct kcallargs *kcaptr;

	if (firstSVCcall)
	{
		// need to init the PCB's.
		int p;
		for (p = 0; p < PROCESSES_COUNT; p++)
		{
			reg_proc(running,&currentPriority,&(plist[p]));
		}
		set_PSP(running[currentPriority] -> sp + 8 * sizeof(unsigned int));

		initMsg();

		firstSVCcall = 0;
		/* Start SysTick */
		setupTick();
		// start uart.
		setupUart();
		// set up messaging

		__asm("	movw 	LR,#0xFFFD");  /* Lower 16 [and clear top 16] */
		__asm("	movt 	LR,#0xFFFF");  /* Upper 16 only */
		__asm("	bx 	LR");          /* Force return to PSP */
	}
	else /* Subsequent SVCs */
	{
		kcaptr = (struct kcallargs *) argptr -> r7;

		switch(kcaptr->code)
		{
		case GETID:
		{
			kcaptr->rtnvalue = kgetID(running,&currentPriority);
			break;
		}
		case NICE:
		{
			knice(kcaptr->arg1,running,&currentPriority);
			break;
		}
		case TERMINATE:
		{
			kterminate(running,&currentPriority);
			break;
		}
		case BIND:
		{
			kcaptr->rtnvalue = kbind(kcaptr->arg1);
			break;
		}
		case UNBIND:
		{
			kcaptr->rtnvalue = kunbind(kcaptr->arg1);
			break;
		}
		case SEND:
		{
			kcaptr->rtnvalue = ksend((msg *)kcaptr->arg1,kcaptr->arg2,running,&currentPriority);
			break;
		}
		case RECV:
		{
			kcaptr->rtnvalue = krecv((msg **)kcaptr->arg1,kcaptr->arg2,running,&currentPriority);
			break;
		}
		case POLL:
		{
			kcaptr->rtnvalue =  kpoll((unsigned int *)kcaptr->arg1,kcaptr->arg2,running,&currentPriority);
			break;
		}
		default:
			//printf("Debug Error: Invalid kernel call code: %u",kcaptr->code);
			;
		}
	}
}

// increments the global time and stopwatch.
// Put this in the interrupt vector table
void systickIsr(void)
{
	IntMasterDisable();
	 // increments clock functions
	systick();
	// Change to next process.
	kcontextSwitch(running,&currentPriority);
	IntMasterEnable();
}
