/*
 * mcmd.h
 *
 *  Created on: Oct 7, 2014
 *      Author: matt
 *
 *      This module is used to handle parsing and looking for commands.
 *      It automatically passes the arguments after a command to it's coresponding function.
 */

#ifndef MCMD_H_
#define MCMD_H_

#define CMD_LENGTH 64

typedef struct
{
	void (*func)(char *); 	/* pointer to function */
	char *name;     	/* text name associated with function */
	int size;		/* max number of chars in name */
}  fentry;

typedef struct
{
	fentry * list;
	int size;
} commandList;


/*
 * Processes a command based on a give command list
 * cmd - a pointer to the command string.
 * cmds - a commandList struct containing the acceptable commands and their functions.
 */
// Note: command must be null terminated.
void processCmd(char * cmd, commandList cmds);

#endif /* MCMD_H_ */
