/*
 * mqueue.h
 *
 *	This module is a queue which is accessed on a single byte basis.
 *	This is satisfactory when dealing with a simple serial comms UART.
 *
 *  Created on: 2014-10-05
 *      Author: Matt Thomson
 */

#ifndef MQUEUE_H_
#define MQUEUE_H_

typedef volatile struct {
	int size;
	volatile char * circbuff;
	volatile int h;
	volatile int t;
} queue;

// Almost a constructor. It init's an already allocated queue struct
/*
 * returns - 0 if the queue was not inited successfully
 * returns - 1 if the queue was initied successfully
 *
 * que - a pointer to an already allocated queue stucture
 * size - the amount of characters that this queue will hold
 */
int initQueue(queue * que, int size);

// Deletes the queue's data space
/*
 * returns - 0 if the queue was not deleted succesfully
 * returns - 1 if the queue was deleted succesfully
 *
 * que - a pointer to a queue stucture
 */
int deleteQueue(queue * que);

// Pushes a byte to the queue
/*
 * returns - 0 if the data could not be added because there is no space in the queue
 * returns - 1 if the data was added to the queue succesfully
 *
 * data - a character to be added to the queue
 * que - a pointer to a queue structure
 */
int enQueue(volatile char data,queue * que);

// Pops a byte from the queue
/*
 * returns - 0 if there is no data in the queue to pop
 * returns - 1 if c was successfully loaded with a poped char
 *
 * c - a pointer to the desired data location. c will be loaded with the poped data
 * que - a pinter to a queue structure
 */
int deQueue(volatile char * c,queue * que);

#endif /* MQUEUE_H_ */
