/*
 * pqueue.h
 *
 *  Created on: Nov 14, 2014
 *      Author: matt
 */

#ifndef PQUEUE_H_
#define PQUEUE_H_

typedef volatile struct {
	int size;
	volatile void ** circbuff;
	volatile int h;
	volatile int t;
} pqueue;

int initpQueue(pqueue * que, int size);

int deletepQueue(pqueue * que);

int enpQueue(volatile void * data,pqueue * que);

int depQueue(volatile void ** data,pqueue * que);

#endif /* PQUEUE_H_ */
