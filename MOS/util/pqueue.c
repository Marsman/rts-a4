/*
 * pqueue.c
 *
 *  Created on: Nov 14, 2014
 *      Author: matt
 */

#include "../devices/mem.h"
#include "pqueue.h"

// You allocate the queue. This sets up the tail and head.
int initpQueue(pqueue * que, int size)
{
	que->circbuff = (void **)allocate(size*sizeof(void *));
	if (que->circbuff != 0)
	{
		que->size = size;
		que->h = 0;
		que->t = 0;
	} else return 0;
	return 1;
}

// delete a queue's memory space
int deletepQueue(pqueue * que)
{
	return deallocate((char *)que->circbuff);
}

// add a data pointer to the queue
int enpQueue(volatile void * data,pqueue * que)
{
	volatile int nexthead = (que->h + sizeof(void *))%que->size;
	if (nexthead==que->t) return 0;
	que->circbuff[que->h] = data;
	que->h = nexthead;
	return 1;
}

// pop a data pointer from the queue
int depQueue(volatile void ** data,pqueue * que)
{
	if (que->t == que->h) return 0;
	data[0] = que->circbuff[que->t];
	que->t = (que->t + sizeof(void *))%que->size;
	return 1;
}
