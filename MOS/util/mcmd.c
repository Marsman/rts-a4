/*
 * mcmd.c
 *
 *  Created on: Oct 7, 2014
 *      Author: matt
 *
 *      This module is used to handle parsing and looking for commands.
 *      It automatically passes the arguments after a command to it's coresponding function.
 *
 */

#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include "mcmd.h"

static void converttoupper(char *lcstr, char *ucstr)
{
	do
		*ucstr++ = toupper(*lcstr);
	while (*lcstr++ != NULL);
}

void processCmd(char * cmd, commandList cmds)
{
	int j;
	char CMD[CMD_LENGTH];
	converttoupper(cmd, CMD);

	// for each possible command
	for(j=0; j<cmds.size; j++)
	{
		/* Case sensitive compare with upper limit on string length */
		if (strncmp(CMD, cmds.list[j] . name,  cmds.list[j] . size) == 0)
		{
			/* CMD match */
			cmds.list[j] . func(&cmd[cmds.list[j] . size]);
			/* Leave the for(j) loop */
			break;
		}
	}
}


