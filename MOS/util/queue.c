/*
 * queue.c
 *
 *	This module is a queue which is accessed on a single byte basis.
 *	This is satisfactory when dealing with a simple serial comms UART.
 *
 *  Created on: 2014-10-05
 *      Author: Matt Thomson
 */

#include "../devices/mem.h"
#include "queue.h"

// You allocate the queue. This sets up the tail and head.
int initQueue(queue * que, int size)
{
	que->circbuff = allocate(size);
	if (que->circbuff != 0)
	{
		que->size = size;
		que->h = 0;
		que->t = 0;
	} else return 0;
	return 1;
}

// delete a queue's memory space
int deleteQueue(queue * que)
{
	return deallocate((char *)que->circbuff);
}

// add a char to the queue
int enQueue(volatile char data,queue * que)
{
	volatile int nexthead = (que->h + 1)%que->size;
	if (nexthead==que->t) return 0;
	que->circbuff[que->h] = data;
	que->h = nexthead;
	return 1;
}

// pop a char from the queue
int deQueue(volatile char * c,queue * que)
{
	if (que->t == que->h) return 0;
	c[0] = que->circbuff[que->t];
	que->t = (que->t + 1)%que->size;
	return 1;
}
