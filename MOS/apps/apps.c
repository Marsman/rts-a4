/*
 * apps.c
 *
 * This section is a testbench for the real time operating system.
 *
 *  Created on: Oct 23, 2014
 *      Author: matt
 */

#include "screen.h"
#include "../kernel/message.h"
#include "../kernel/system.h"
#include "../deviceWrappers/mtime.h"


void testSend(void)
{
	msg testmsg;
	testmsg.size = 12;
	char str[] = "Hello World\0";
	testmsg.data = str;

	do {
		;
	} while (send(&testmsg,1) != MSG_SUCCESS);

	while (1) {
		;
	}

}

void testPoll(void)
{
	msg * testmsg;
	if (bind(1) == MSG_SUCCESS)
	{
		unsigned int pollList[2] = {0,1};


		poll(pollList,2);

		if (recv(&testmsg,1) == MSG_SUCCESS)
		{
			if (send(testmsg,0) == MSG_SUCCESS)
			{

			}
		}

	}
}

void testReceive(void)
{
	msg * testmsg;
	if (bind(0) == MSG_SUCCESS)
	{
		do {
			;
		} while (recv(&testmsg,0) != MSG_SUCCESS);

		AtomicPrintPosition(1,1,testmsg->data);
	}
	while (1){
	;
	}
}

// basic process that prints it's PID.
void pidShower(void)
{
	unsigned int pid = getID();
	AtomicPrintPosition(2*pid+1,0,"This is Process \0");
	char cpid[2];
	cpid[0]= ((unsigned char)pid+'0');
	cpid[1]= 0;
	AtomicPrintPosition(2*pid+1,20,cpid);
}

// A process that shows the system running time.
void Priority3SW(void)
{
	unsigned int pid = getID();
	AtomicPrintPosition(2*pid+1,0,"Stopwatch priority \0");
	char cpri[2];
	cpri[0]= (3+'0');
	cpri[1]= 0;
	AtomicPrintPosition(2*pid+1,20,cpri);
	resetStopWatch();
	char * stopWatchStr;

	int i = 0;
	while (i < 100){ // run
		// update the stopwatch string
		stopWatchStr = getStopWatchStr();
		// getStopWatchStr will return null if its value has not changed.
		if (stopWatchStr != 0)
		{
			i++;
			AtomicPrintPosition(2*pid+2,0,stopWatchStr);
		}
	}
}


// A process that counts up and then changes priority. This was process A in the testing.
void NicingSW(void)
{
	unsigned int pid = getID();
	unsigned int pri = 3;
	AtomicPrintPosition(2*pid+1,0,"Stopwatch priority \0");
	char cpri[2];
	cpri[0]= ((unsigned char)pri+'0');
	cpri[1]= 0;
	AtomicPrintPosition(2*pid+1,20,cpri);

	resetStopWatch();
	char * stopWatchStr;

	int i = 0;
	while (i < 15){ // run
		// update the stopwatch string
		stopWatchStr = getStopWatchStr();
		// getStopWatchStr will return null if its value has not changed.
		if (stopWatchStr != 0)
		{
			i++;
			AtomicPrintPosition(2*pid+2,0,stopWatchStr);
		}
	}
	pri ++;
	nice(pri);
	cpri[0]= ((unsigned char)pri+'0');
	AtomicPrintPosition(2*pid+1,20,cpri);
	i = 0;
	while (i < 100){ // run
			// update the stopwatch string
		stopWatchStr = getStopWatchStr();
			// getStopWatchStr will return null if its value has not changed.
		if (stopWatchStr != 0)
		{
			i++;
			AtomicPrintPosition(2*pid+2,0,stopWatchStr);
		}
	}


	// character c has terminated the go command and is thrown out.
}


// The idle process that is to be placed with priority 0.
void idle(void)
{
	// clean screen
	writeChar(0x1b);
	writeString("[2J");
	writeChar(0x1b);
	writeString("[H");
	char str[] ="/-\\|";
	unsigned int i = 0;

	// display a spinning character.
	while (1){
		if(getStopWatchStr() != 0){
			i++;
			writeChar(0x08);
			writeChar(str[i%4]);
		}
	};
}
