/*
 * apps.h
 *
 *  Created on: Oct 23, 2014
 *      Author: matt
 */

#include "../kernel/pinfo.h"
#include "../../USPACE/Trainset/trainsetinit.h"
// Include process structure and process count values

#ifndef APPS_H_
#define APPS_H_
#ifndef TRAINSETINIT_H_
void proc1(void);
void NicingSW(void);
void Priority3SW(void);
void idle(void);

void testSend(void);
void testReceive(void);
void testPoll(void);


#define PROCESSES_COUNT 4

// The list of processes that will be mounted at startup.
struct processInfo plist[] = {
		{0,0,idle}
		,{1,3,testSend}
		,{2,3,testReceive}
		,{3,3,testPoll}
};

#endif

#endif /* APPS_H_ */
