/*
 * trainsetinit.h
 *
 *  Created on: 2014-11-21
 *      Author: William
 *
 * extension of Matts init structures allowing me to adjust it in USPACE
 */

#ifndef TRAINSETINIT_H_
#define TRAINSETINIT_H_
#include "routing.h"
void MainScreenProcess(); 		// priority 4 - worried about printing being interrupted by 'other things'
void TrainControllerProcess(); 	// priority 3 - reset the train state
void OutputProcess();			// priority 3 - allow other processes to output during initialization if desired
void ConsoleProcess();			// priority 2 - allow for io
void MapProcess();				// priority 1 - draw out the map
void WaitingProcess();			// priority 0 - nothing is going on, idle

// The list of processes that will be mounted at startup.
#define PROCESSES_COUNT 12
struct processInfo plist[] = {
		 {0,0,WaitingProcess}
		,{1,4,MainScreenProcess}
		,{2,3,ConsoleProcess}
		,{3,2,MapProcess}
		,{4,4,TrainControllerProcess}
		,{5,3,OutputProcess}
		,{6,2,train1}
		,{7,2,train2}
		,{8,3,control_T1}
		,{9,3,control_T2}
		,{10,3,HS_handler}
		,{11,3,update_state}
};

#endif /* TRAINSETINIT_H_ */
