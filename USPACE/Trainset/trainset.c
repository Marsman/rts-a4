/*
 * Trains.c
 *
 *  Created on: 2014-11-13
 *      Author: William
 *
 *		A collection of user processes which manage the trainset.
 *
 *      Creates console process,
 *      Creates map process,
 *      Creates ATmel process, which
 *      	has hall sensors,
 *      	has switches,
 *      	has track segment control,
 *      	receives hall sensor portf interrupts,
 *
 *      uses GUI stuff
 *
 *      Eventually will need to be extended to work for multiple trains.
 *      Eventually may be used to gather data on different speeds and times and stuff.
 *
 */

#include "trainset.h"

/*************************************************************
 * 							GENERAL
 *************************************************************/
void printmessage(gui* gui, char* str, cursor_pos print_pos){
/* send it off to OS, from whichever GUI */
	int i = 0;
	printmsg pmsg;
	msgReturnCode rtn;
	pmsg.str = str;
	pmsg.targetGUI = gui;
	pmsg.print_pos = print_pos;
	rtn = sendmsg((char*) &pmsg, PRINT_PORT, sizeof(printmsg));

	// TODO: add bind which wakes up the process when it can run.
	if(rtn != MSG_SUCCESS)
		while(i < 1000) i++;
}

void WaitingProcess(){
/* idle function burns clock-cycles */
	while(1);
}

/*************************************************************
 * 							CONSOLE
 *************************************************************/
/* Console still requires some kind of argument parsing function
 * Besides that, the basics are essentially done.
 * Still need to create actual console commands
 * Probably tons of errors. */

#define UI_CONSOLE_TOP		36
#define UI_CONSOLE_LEFT 	1
#define UI_CONSOLE_WIDTH	34
#define UI_CONSOLE_HEIGHT	9
#define UI_CONSOLE_NAME "Locomotive Console\0"

void InitConsoleProcess(gui*);
char consolerecv();
int strncmpi(char* str1, char* str2, unsigned int len);
void command_parse(char* cmd, unsigned int* cmd_inx);
void command_gen(char* cmd,unsigned int* cmd_inx,char rx);

void ConsoleProcess(){
/* Receive characters from the UART, store them in a string and transmit them out */
char cmd[MAXCMDLEN];
char rx[2];
unsigned int cmd_inx = 0;
unsigned int recvport = CONSOLE_PORT;
gui ConsoleGUI;
ConsoleGUI.process_area.left = UI_CONSOLE_LEFT;
ConsoleGUI.process_area.top = UI_CONSOLE_TOP;
ConsoleGUI.process_area.width = UI_CONSOLE_WIDTH;
ConsoleGUI.process_area.height = UI_CONSOLE_HEIGHT;

InitConsoleProcess(&ConsoleGUI);

rx[1] = (char)NULL;
while(1){
/* receive characters and parse incoming commands */
	poll(&recvport, 1);
	rx[0] = consolerecv();
	command_gen(cmd, &cmd_inx, rx[0]);
	printmessage(&ConsoleGUI, rx, ConsoleGUI.text.rel_pos);
}
} /* Console Process */

void InitConsoleProcess(gui* ConsoleGUI){
/* initialize the GUI and set this as the typing process */
	/* set this process as the typing process */
	tswitchmsg tsmsg;

	tsmsg.portid = CONSOLE_PORT;
	bind(CONSOLE_PORT);

	sendmsg((char*) &tsmsg, SWITCHACTIVETYPING_PORT, sizeof(tswitchmsg));

	/* create a GUI for this process */
	newguimsg guimsg;

	guimsg.newGUI = ConsoleGUI;
	guimsg.processarea	= ConsoleGUI->process_area;
	guimsg.name = UI_CONSOLE_NAME;

	sendmsg((char*) &guimsg, NEWUI_PORT, sizeof(newguimsg));
}

char consolerecv(){
/* get the character out of the message */
	U0rm* crm = (U0rm*) recvmsg(CONSOLE_PORT);
	char rtn;
	if(crm != NO_MSG){
		rtn = crm->chr;
		del((msg*)crm);
		return rtn;
		/* done with the message */
	}
	while(1);
	return '\0';
	/* should never be here, since it polls only on this port */
}

char toupper(char chr){
/* takes a character and returns the uppercase version if it is not uppercase already */
	return (chr >= 'a') ? (chr + ('A'-'a')) : chr;
}

int strncmpi(char* str1, char* str2, unsigned int len){
/* String comparison function. returns 0 if two strings are identical over n characters.
 * returns +ve if str1[i] > str2[i], -ve if str1[i] < str2[i],
 * for the first i which either is true */
	int i = 0, hold = 0;
	for(;i < len; i++){
		/* go through the strings and look for a mismatch in hold */
			hold = (int)toupper(*(str1+i)) - (int)toupper(*(str2+i));
			if(hold != 0){
				/* if hold isn't = 0, return hold */
				return hold;
			}
	} /* all characters over n characters are equal */
	return hold;
}

void command_parse(char* cmd, unsigned int* cmd_inx){
/* see if the passed string matches any possible commands */
/* other copied function for comparing to commands */
	int j;
	/* compare the strings, and ensure cmd has either a space or a null character at
	 * cmd_options[].len. e.g. inherient command or command[space]args */
	for(j=0;j < NO_OF_CMD; j++){
		if(strncmpi(cmd_options[j].name,cmd,cmd_options[j].len)==0 && 	// command string = stored string
				((*(cmd+cmd_options[j].len) == NULL) ||			  		// command string is terminated by \0 OR [no args]
				(*(cmd+cmd_options[j].len) == __CHR_SPACE))){			// command string is terminated by SPACE [more args]
			*cmd_inx = 0;
			cmd_options[j].func(cmd);
			break;
		}
		if(j == NO_OF_CMD - 1) {outputstr("Invalid/Unrecognized command");}
	} /* either the command has been called or no command matched at this point */
}

#define CMDMAXLEN 24
void command_gen(char* cmd,unsigned int* cmd_inx,char rx){
	/* check for a command that corresponds to a console command */
	switch(rx)
	{
	/* command handling */
	case BS:
		/* if the character is a backspace, move back one space in command string
		 * [char] [char] [empty] <- cmd_inx
		 * to
		 * [char] [char] <-cmd_inx */
		if(*cmd_inx != 0) {(*cmd_inx)--;}
	break;
	case RETURN:
		/* LINEFEED indicates end of string */
		cmd[(*cmd_inx)++%(CMDMAXLEN + 1)] = NULL;
	break;
	default:
		/* Default case, place rx into the cmd string */
		cmd[(*cmd_inx)++%(CMDMAXLEN + 1)] = rx;
	break;
	}

	/* avoid checking a [-1] index */
	if(*cmd_inx > 0){
		/* perform command checking by seeing if a return character has been replaced by null in the command */
		if(cmd[*cmd_inx-1] == NULL){
			if(*cmd_inx < (CMDMAXLEN + 1)){
				/* cmd COULD be a valid command. Check and if it matches any known commands */
				command_parse(cmd,cmd_inx);
			}
			/* reset the cmd_inx */
			*cmd_inx = 0;
		}
	}
	/* command handling complete */
}

unsigned char parsestrtochar(char* str, unsigned int firstdigit, unsigned int lastdigit){
/* returns the number representation of a string number */
unsigned char k = lastdigit, rtn = 0;
unsigned char j = 1;
unsigned char f_temp=0;
if(firstdigit=='-')
{
	f_temp=1;//indicate it is negative
	firstdigit = 0;
}
while(k >= firstdigit){
	if(*(str+k) >= '0' && *(str+k) <= '9'){
	rtn += (*(str+k) - '0')*j;
	j*=10;
	k--;
	} else {
		while(1); // find out what went wrong
	}
}
if(f_temp)
	return -rtn;
else
	return rtn;
}

// TODO: Test track function should validate input
void TestTrack(char* cmd_str){
/* Sends a message to the track handler to change the track speed
 * command in the form of 'TEST TRACK ## #\0'
 * first parameter is the id of the track
 * second paramter is the new velocity of the track (-7 to +7 valid) */
	trackstate trackmsg;
	msgReturnCode rtn;
	trackmsg.type = TRACK;
	trackmsg.id = parsestrtochar(cmd_str, 11,12);
	trackmsg.arg = parsestrtochar(cmd_str, 14,14);
	rtn = sendmsg((char*) &trackmsg, TRACKACTION_PORT, sizeof(trackmsg));
	outputstr("Testing Track...");
}

// TODO: Test switch function should validate input
void TestSwitch(char* cmd_str){
/* Sends a message to the track handler to change the track speed
 * command in the form of 'SWITCH # #\0'
 * first parameter is the id of the switch to test
 * second parameter is the new switch state: 0 = turn, 1 = straight */
	trackstate switchmsg;
	msgReturnCode rtn;
	switchmsg.type = SWITCH;
	switchmsg.id = parsestrtochar(cmd_str, 7,7);
	switchmsg.arg = parsestrtochar(cmd_str, 9,9);
	rtn = sendmsg((char*) &switchmsg, TRACKACTION_PORT, sizeof(switchmsg));
	outputstr("Testing switch...");
}

#define NUMBEROFSEGMENTS 16
#define UNINITIALIZED 0
tmsg train1_params = {UNINITIALIZED,UNINITIALIZED,UNINITIALIZED,UNINITIALIZED};
tmsg train2_params = {UNINITIALIZED,UNINITIALIZED,UNINITIALIZED,UNINITIALIZED};
void InitTrain(char* cmd_str){
/* a message initialise the train. does not send off the train messages
 * TRAIN A BB CC
 * AA = 1, 2 = train id
 * BB = starting section
 * CC = dst section */
	int train = parsestrtochar(cmd_str, 6,6);
	int loc = parsestrtochar(cmd_str, 8,9);
	int dst = parsestrtochar(cmd_str, 11,12);
	//int spd = parsestrtochar(cmd_str, 14,15);
	if(train == 1 && loc > 0 && loc <= NUMBEROFSEGMENTS && dst > 0 && dst <= NUMBEROFSEGMENTS){
		train1_params.start_section = loc;
		train1_params.destination_section = dst;
		//train1_params.speed=spd;
		outputstr("Train 1 Initialized...");
	} else if (train == 2 && loc > 0 && loc <= NUMBEROFSEGMENTS && dst > 0 && dst <= NUMBEROFSEGMENTS){
		train2_params.start_section = loc;
		train2_params.destination_section = dst;
		//train1_params.speed=spd;
		outputstr("Train 2 Initialized...");
	} else {
		/* invalid train */
		/* TODO: output area for text maybe */
		outputstr("Invalid train cmd");
	}
}

void Start(char* unused){
/* send the train messages to declan to initialize movement */
msgReturnCode rtn1 = MSG_FAIL, rtn2 = MSG_FAIL;
if(train1_params.destination_section != UNINITIALIZED){
	rtn1 = sendmsg((char*) &train1_params, TRAIN1_CMDS, sizeof(tmsg));
}

if(train2_params.destination_section != UNINITIALIZED){
	rtn2 = sendmsg((char*) &train2_params, TRAIN2_CMDS, sizeof(tmsg));
}

if(rtn1 == MSG_SUCCESS){
	outputstr("Train 1 started!!");
}

if(rtn2 == MSG_SUCCESS){
	outputstr("Train 2 started!!");
}

if(rtn1 == MSG_FAIL && rtn2 == MSG_FAIL){
	outputstr("Unable to start trains...");
}
}

/*************************************************************
 * 							OUTPUT
 *************************************************************/

#define UI_OUTPUT_TOP		36
#define UI_OUTPUT_LEFT 		34
#define UI_OUTPUT_WIDTH		66
#define UI_OUTPUT_HEIGHT	9
#define UI_OUTPUT_NAME "Locomotive Output\0"

#define NEW_LINE "�n\0"

void InitOutputProcess(gui*);
void OutputProcess(){
/* Receive characters from the map and controller and puts the text in the output */
gui OutputGUI;
unsigned int recvport = OUTPUT_PORT;
char* output_str;
OutputGUI.process_area.left = UI_OUTPUT_LEFT;
OutputGUI.process_area.top = UI_OUTPUT_TOP;
OutputGUI.process_area.width = UI_OUTPUT_WIDTH;
OutputGUI.process_area.height = UI_OUTPUT_HEIGHT;

InitOutputProcess(&OutputGUI);

while(1){
/* receive characters and parse incoming commands */
	poll(&recvport, 1);
	output_str = recvmsg(recvport);
	if(output_str != NO_MSG){
		printmessage(&OutputGUI, output_str, OutputGUI.text.rel_pos);
		printmessage(&OutputGUI, NEW_LINE, OutputGUI.text.rel_pos);
	}
}
} /* OUTPUT Process */

void InitOutputProcess(gui* OutputGUI){
/* initialize the GUI and binds to the port */
	bind(OUTPUT_PORT);

	/* create a GUI for this process */
	newguimsg guimsg;

	guimsg.newGUI = OutputGUI;
	guimsg.processarea	= OutputGUI->process_area;
	guimsg.name = UI_OUTPUT_NAME;

	sendmsg((char*) &guimsg, NEWUI_PORT, sizeof(newguimsg));
}

void outputstr(char* str){
/* send the string to the output port. determine the length of the string and send it. */
	int i = 0;
	while(*(str+i) != '\0') {i++;}
	sendmsg(str,OUTPUT_PORT, sizeof(char)*(i+1));
	// +1 to include '\0' in string
}

/*************************************************************
 * 							MAP
 *************************************************************/
#define UI_MAP_TOP		1
#define UI_MAP_LEFT 	1
#define UI_MAP_WIDTH	99
#define UI_MAP_HEIGHT	37
#define UI_MAP_NAME "Locomotive Map\0"
#define INIT_COMPLETE_MSG "LOCO initialized successfully!�nCreated by: Will, Matt and Declan�ncmds:[NEW TRAIN] A BB CC {id, loc, dst}, [START] start trains�nEx:NEW TRAIN 1 01 16"
/* TODO: remove this maptext area, unless you decide you want to use it later
#define UI_MAPTEXT_TOP		40
#define UI_MAPTEXT_LEFT 	50
#define UI_MAPTEXT_WIDTH	49
#define UI_MAPTEXT_HEIGHT	7*/

void InitMap();
void UpdateHall(gui*, usmsg*);
void UpdateVelocity(gui*,usmsg*);
void UpdateSwitch(gui*,usmsg*);

void MapProcess(){
gui MapGUI;
usmsg* updatemapsymbol;
unsigned int map_port = MAP_PORT;
void (*UpdateMapSymbol[])(gui*,usmsg*) = {
	UpdateHall,
	UpdateVelocity,
	UpdateSwitch
};
MapGUI.process_area.left = UI_MAP_LEFT;
MapGUI.process_area.top = UI_MAP_TOP;
MapGUI.process_area.width = UI_MAP_WIDTH;
MapGUI.process_area.height = UI_MAP_HEIGHT;
/* initializes the user interface. besides requesting the area, everything else is handled inside the OS */

/* initialize the sensors, switches and track segment variables */
InitMap(&MapGUI);
#ifdef TEST
TestUpdateHall(&MapGUI, 26 /*top left*/, 0 /* 1 */);
#endif
while(1){
	/* receive messages from train controller process */
	poll(&map_port, 1);
	updatemapsymbol = (usmsg*) recvmsg(MAP_PORT);
	// Now have the event; can call three seperate functions.
	UpdateMapSymbol[updatemapsymbol->msg_type](&MapGUI, updatemapsymbol);
	// At this point the map symbol has been updated. Free the message
	del((msg*)updatemapsymbol);
}
}

#define HALL_STR_C "o\0"
#define HALL_STR_S "x\0"
void UpdateHall(gui* map,usmsg* hallmsg){
/* update the hall sensor and update the associated train space text area */
/* reset previous hall sensor */
char section[total_HS+1]={0,1,1,2,2,3,3,4,4,5,5,6,6,7,7,8,8,9,9,
			10,10,11,11,12,12,13,13,14,14,15,15,16,16};
// easiest way of fixing this quickly
char trainstr[] = {'0','0','\0'};
if(prev_hall != 0){
	printmessage(map, HALL_STR_C, halltable[prev_hall]);
}
prev_hall = hallmsg->sym_id;
printmessage(map, HALL_STR_S, halltable[hallmsg->sym_id]);
trainstr[0] = section[hallmsg->sym_id]/10 + '0';
trainstr[1] = section[hallmsg->sym_id]%10 + '0';
printmessage(map, trainstr, traintable[hallmsg->train].loc);
}

#define CLOCK "CLK\0"
#define COUNTERCLOCK "CCK\0"
#define NOSPD "---\0"
#define ABS(x) (x|0x8f)
void UpdateVelocity (gui* map,usmsg* hallmsg){
/* update the trainspace text area */
char trainstr[] = {'0','0','\0'};
char aspd;
if(hallmsg->speed > 0){
	printmessage(map, CLOCK, traintable[hallmsg->train].dir);
} else if (hallmsg->speed < 0){
	printmessage(map, COUNTERCLOCK, traintable[hallmsg->train].dir);
} else {
	printmessage(map, NOSPD, traintable[hallmsg->train].dir);
}

// get the abs val of the speed for the screen
aspd = ABS(hallmsg->speed);
trainstr[0] += aspd/10;
trainstr[1] += aspd%10;
printmessage(map, trainstr, traintable[hallmsg->train].spd);
}

#define STR_TRACK_TURNED "TRN\0"
#define STR_TRACK_STRGHT "STR\0"
void UpdateSwitch(gui* map,usmsg* hallmsg){
/* update the switch graphic
 * TODO: improve this if else for the update switch function */
if(hallmsg->state == ATMEL_TRACK_TURNED){
	printmessage(map, STR_TRACK_TURNED, switchtable[hallmsg->sym_id]);
} else if (hallmsg->state == ATMEL_TRACK_STRAIGHT){
	printmessage(map, STR_TRACK_STRGHT, switchtable[hallmsg->sym_id]);
} else {
	// Error has occurred.
	while(1);
}
}

void InitMap(gui* MapGUI){
/* draw out the map row by row. Then initialize switches, track segments and clear hallsensors */
	/* create a GUI for this process */
	newguimsg guimsg;

	guimsg.newGUI = MapGUI;
	guimsg.processarea	= MapGUI->process_area;
	guimsg.name = UI_MAP_NAME;

	sendmsg((char*) &guimsg, NEWUI_PORT, sizeof(guimsg));

	printmessage(MapGUI, init_map, MapGUI->text.rel_pos);

	bind(MAP_PORT);

	outputstr(INIT_COMPLETE_MSG);
}

/*************************************************************
 * 					TRAINSET INTERFACE
 *************************************************************/
#define DIR(x) (((x & 0x80) == 0) ? _FORWARD : _REVERSE)
#define CONSOLE_FN 2
void InitTrainController();
void hall_message();
void track_message();
void TrainControllerProcess(){
/* Process which manages state changes and control
 * receives messages from: Hall Sensor Indication
 * Sends messages to: Map Process */
	char* data;
	int i;

	InitTrainController();
	unsigned int controller_ports[] = {
		TRAINCONTROLLER_PORT,
		TRACKACTION_PORT
	};
	void (*msghandler[])(char *)={
			hall_message,
			track_message
	};

	while(1){
		poll(controller_ports, 2);
		for(i = 0; i < CONSOLE_FN; i++){
			data = recvmsg(controller_ports[i]);
			if(data != NO_MSG){
				msghandler[i](data);
				del((msg*) data);
			}
		}
	}
}

// TODO: actual speed implementation
int v = 0;
char update_console[] = {'E','V','E','N','T',':','H','S',' ','0','0','\0'};
#define HALL_TENS 9
#define HALL_ONES 10
void hall_message(char* data){
	halltrigger* rcvhall;
	rcvhall = (halltrigger*) data;
	/* have message, tell OS to update this hall sensor */
	usmsg upsymmsg;
	char hall_id;

	hall_id = PollHallSensor(rcvhall->atmel);

	update_console[HALL_TENS] = hall_id / 10 + '0';
	update_console[HALL_ONES] = hall_id % 10 + '0';

	outputstr(update_console);

	upsymmsg.msg_type = HALL;
	upsymmsg.sym_id = hall_id;
	sendmsg((char*) &upsymmsg, MAP_PORT, sizeof(upsymmsg));

	// sending it out to the train processes
	tmsg hall_send;
	hall_send.HS=(unsigned)hall_id;
	sendmsg((char*) &hall_send, HS_DATA, sizeof(usmsg));
	//sendmsg((char*) &hall_id, TRAIN1_HS, sizeof(usmsg));
}

void track_message(char* data){
	// Launch the appropriate function given the message request
	usmsg update_map_symbol;
	trackstate* trackchange = (trackstate*) data;
	if(trackchange->type == SWITCH){
		SwitchToState(trackchange->id, trackchange->arg);

		// update the map switch symbol
		update_map_symbol.msg_type = SWITCH;
		update_map_symbol.sym_id = trackchange->id;
		update_map_symbol.state = trackchange->arg;
		// TODO: remove this hardcoded thing
		update_map_symbol.train	= 1;
		sendmsg((char*) &update_map_symbol, MAP_PORT, sizeof(usmsg));
	}

	if(trackchange->type == TRACK){
		SegmentVelocity(trackchange->id, trackchange->arg);

		// no map changes
	}
}

void InitTrainController(){
/* bind to port, reset the hall sensors, set all sections to 0 */
	bind(TRAINCONTROLLER_PORT);
	bind(TRACKACTION_PORT);
	SPI_Init();
	poffall();
	ResetAllHallSensors();
	ResetHallSensorQueue();
	ResetSwitches();

	PORTF_Init();
}


/*************************************************************
 *						INTERRUPTS
 *************************************************************/

// Must be added to startup_ccs.c's interrupt vector table
void GPIOPortF_IntHandler(void)
{
/* Taken from gpiof.h, credit to the Author: Emad Khan. Other functions can be found in gpiof.h */
// Configured for Rising Edge triggered interrupts on Pins 0 and 1
// Create hall sensors
halltrigger hall;

// Clear the two interrupts
PORTF_GPIOICR_R -> IC |= (1<<BIT0) | (1<<BIT1);

if((PORTF_GPIODR_R -> DATA & 0x01) == 0x01)
{
// This indicates that Hall Sensors 1 - 8 has been triggered
// And that Atmel board #1 has signalled the Cortex via Pin 0 on PORTF
// The application can now process the interrupt (e.g. fetch the HS# via SPI)
	hall.atmel = ATMEL1;
	hsendmsg((char*) &hall, TRAINCONTROLLER_PORT, sizeof(halltrigger));
}

if((PORTF_GPIODR_R -> DATA & 0x02) == 0x02)
{
// This indicates that Hall Sensors 9 - 32 has been triggered
// And that Atmel board #3 has signalled the Cortex via Pin 1 on PORTF
// The application can now process the interrupt (e.g. fetch the HS# via SPI)
	hall.atmel = ATMEL3;
	hsendmsg((char*) &hall, TRAINCONTROLLER_PORT, sizeof(halltrigger));
}

}
