/*
 * Trains.h
 *
 *  Created on: 2014-11-13
 *      Author: William
 */

#ifndef TRAINS_H_
#define TRAINS_H_

#include "../Message/trainmessage.h"
#include "../ATmel/gpiof.h"
#include "../ATmel/spi_train.h"
#include "routing.h"


#ifdef TEST_UART
#include "../../MOS/kernel/kernel.h"
#include "../../MOS/kernel/kprocess.h"
#endif

/*************
 * PROCESSES *
 *************/

void ConsoleProcess();
void MapProcess();
void TrainControllerProcess();
void WaitingProcess();


/**************
 * CONTROLLER *
 **************/


/***********
 * CONSOLE *
 ***********/
extern void assignR7(unsigned long);
extern void SVCall();

#define RETURN 		13
#define BS			127
#define MAXCMDLEN 	32

struct command
{
	char* name;
	void (*func)(char*);
	unsigned int len;
}; /* struct command */

void TestTrack(char*);
void TestSwitch(char*);
void Start(char*);
void InitTrain(char*);
#define NO_OF_CMD 4
struct command cmd_options[] = {
		{"TRACK", 	TestTrack, 	5},
		{"SWITCH", TestSwitch, 6},
		{"TRAIN", 	InitTrain, 	5},
		{"START", 		Start,	5}
};

// TODO: write test commands and initialisation commands

/**********
 * OUTPUT *
 **********/

void outputstr(char*);

/*******
 * MAP *
 *******/

/* forward when inside of the track is on the right side, backwards when inside of the track is on the left side */

unsigned int prev_hall = 0;
cursor_pos halltable[] = {
		{0,0}, 		// HS0
		{85,26},	// HS1
		{85,5},		// HS2
		{66,3},		// HS3
		{51,3},		// HS4
		{45,3},		// HS5
		{32,3},		// HS6
		{11,5},		// HS7
		{11,25},	// HS8
		{31,28},	// HS9
		{48,28},	// HS10
		{56,28},	// HS11
		{78,28},	// HS12
		{79,23},	// HS13
		{81,8},		// HS14
		{71,6},		// HS15
		{51,6},		// HS16
		{45,6},		// HS17
		{24,6},		// HS18
		{16,8},		// HS19
		{18,23},	// HS20
		{24,25},	// HS21
		{37,25},	// HS22
		{43,25},	// HS23
		{67,25},	// HS24
		{65,0},		// HS25
		{32,0},		// HS26
		{47,9},		// HS27
		{23,10},	// HS28
		{64,22},	// HS29
		{35,22},	// HS30
		{64,22},	// HS31
		{53,31},	// HS32
};
// TODO: fill halltable

cursor_pos switchtable[] = {
		{	0	,	0	},			// NULL Switch
		{	71	,	4	},			// Sw 1
		{	61	,	2	},			// Sw 2
		{	38	,	2	},			// Sw 3
		{	26	,	4	},			// Sw 4
		{	25	,	27	},			// Sw 5
		{	43	,	29	},			// Sw 6
		{	41	,	7	},			// Sw 7
		{	46	,	24	},			// Sw 8
		{	70	,	26	},			// Sw 9
};

/* EXTERN VARS */

/* EXTERN FN */
extern SPI_Init();

typedef struct trainspace{
	cursor_pos loc;
	cursor_pos dst;
	cursor_pos spd;
	cursor_pos dir;
}trainspace;

trainspace traintable[] = {
		{{40, 16},{48,16},{40,17},{48,17}},
		{{59,16},{67,16},{59,17},{67,17}}
};

// map string
char* init_map = "                             /==o================================o==��n                            //                [13]                   ���n                           //         STR                    STR      ���n              /===========S4====o=====S3=====o==|==o=========S2===o====S1==========��n            //     [3]    STR          ��                   //         STR  [2]    ���n           o                            ��                 //                        o�n         //       /=====o================S7==o==|==o======XX===========o=======�      ���n        //       //            [9]       STR             //  [8]               ��       ���n       //       o                                 /====/                         o       ���n      //      //              /================o==/                                ��      ���n     //      //     /==o======/         [14]                                        ��      ���n    //      //                                                                       ��      ���n   ||      //                                                                         ��     ||�n  ||       ||               OO                                                        ||     ||�n  ||       ||             oo      __________________ __________________               ||     ||�n  ||       ||            o ______ |TRAIN 1 STATS   | |TRAIN 2 STATS   |               ||     ||�n  ||[4]    ||[10]      _()_||__|| |LOC: 00 DST: 00 | |LOC: 00 DST: 00 |            [7]||  [1]||�n  ||       ||         (         | |SPD: 00 DIR: FWD| |SPD: 00 DIR: REV|               ||     ||�n  ||       ||        /-OO----OO**=*OO------------OO*=*OO------------OO*             ||      ||�n   ||      ||        ###################################################            ||      ||�n   ||       ��                                                                     //       ||�n     ��        ��                             [15]                               //        //�n      ��        ��              ===o============================o==�            //        //�n       ��         o                                                 ��         o         //�n        ��         ��         [11]            STR       [12]         ��     //         //�n           o         �==o============o==|==o==S8===================o==S9===/          //�n            ��                               //                       STR            o�n              ��         STR  [5]           //          [6]                        //�n               �=========S5====o===========S6===o===|===o=====================o===/�n                          ��               STR�n                           ��             [16]�n                            �====o===================o===|\0";

#endif /* TRAINS_H_ */
