/*
 * routing.h
 *
 *  Created on: 2014-11-25
 *      Author: Declan
 */

#ifndef ROUTING_H_
#define ROUTING_H_
#include "../Message/trainmessagestructs.h"
struct movement{
	unsigned char sw;
	unsigned char sw_action; //1=straight;0=thrown
	unsigned dir;//1=cw, 0=ccw
	unsigned char next;
};

#define total_sections 16
#define cw 1
#define ccw 0
#define straight 1
#define thrown 0
#define none -1
#define total_HS 32
#define std_speed 4
#define total_switches 9
#define train_1 0
#define train_2 1
#define max_control_functions 4
unsigned char move(unsigned char current, unsigned char target,
		unsigned int* portlist,unsigned train_number);

unsigned get_hs(unsigned int* portlist);
unsigned char get_section(unsigned hs);
unsigned char virtual_machine(unsigned dir,unsigned int* portlist);
void train1();
void train2();

void control_T1();
void control_T2();
void HS_handler();
void update_state();
void controller();
void check_collision();

#endif /* ROUTING_H_ */
