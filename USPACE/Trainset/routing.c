/*
 * routing.c
 *
 *  Created on: 2014-11-25
 *      Author: Declan
 */
#include "routing.h"
#include <stdio.h>
#include "../ATmel/spi_train.h"
#include "../Message/trainmessage.h"
#include "../Message/trainmessageports.h"

// write out to the output space
extern void outputstr(char*);

/************************************************************************************************************************
 * HOW TO INTERFACE WITH OTHER STUFF
 ************************************************************************************************************************
 * Alright, so you want to do some things. Here's how you do them!
 *
 * To output to the screen, use outputstr(char*) seen above; be sure to null terminate your strings!
 *
 * To receive your train state, bind to ports TRAIN1_CMDS and TRAIN2_CMDS respectively and then poll I guess.
 * I am sending the initial train structures here.
 *
 * To receive hall sensor information, bind to ports TRAIN1_HS and TRAIN2_HS then poll. TODO: forward HS to these interrupts once PortF works
 *
 * To change the switches or the track speed, send a message to TRACKACTION_PORT
 *
 * TRACKACTION_PORT expects data to be a pointer to a struct of type 'trackstate' (found in trainmessagestructs.h)
 * trackstate
 * {
 * type = {SWITCH OR TRACK}
 * id = id of the switch or track of interest. 1-9 or 1-16
 * arg = the new state. +7 to -7 for track (with +ve values being clockwise and -ve values being cclockwise)
 * OR
 * arg = the new state. 0 is thrown, 1 is straight
 * }
 * reminder: enum {HALL = 0, SWITCH = 1, TRACK = 2}
 *
 *
 ***************************************************/

//struct movement{
//	unsigned char sw;
//	char sw_action;//1=straight;0=thrown
//	unsigned dir;//1=cw, 0=ccw
//	unsigned char next;
//};
//0=1 1=2 etc etc, decrement before putting into table..
struct movement action[total_sections][total_sections] =
		{
		/*From*/
		/*1*/{ { 0, 0, 0, 0 }, { 1, straight, ccw, 2 }, { 1, straight, ccw, 2 },
				{ 1, straight, ccw, 2 }, { none, none, cw, 6 }, { none, none,
						cw, 6 }, { 1, straight, ccw, 2 }, { none, none, cw, 6 },
				{ 1, straight, ccw, 2 }, { 1, straight, ccw, 2 }, { 1, straight,
						ccw, 2 }, { 1, straight, ccw, 2 },
				{ 1, thrown, ccw, 2 }, { 1, straight, ccw, 2 }, { 1, straight,
						ccw, 2 }, { none, none, cw, 6 } },

		/*2*/{ { 2, straight, cw, 1 }, { 0, 0, 0, 0 }, { 2, straight, ccw, 3 },
				{ 2, straight, ccw, 3 }, { 2, straight, cw, 1 }, { 2, straight,
						cw, 1 }, { 2, straight, ccw, 3 },
				{ 2, straight, cw, 1 }, { 2, straight, cw, 1 }, { 2, straight,
						cw, 1 }, { 2, straight, ccw, 3 },
				{ 2, straight, ccw, 3 }, { 2, straight, cw, 1 }, { 2, thrown,
						ccw, 14 }, { 2, straight, ccw, 3 }, { 2, straight, ccw,
						3 } },

		/*3*/{ { 3, straight, cw, 2 }, { 3, straight, cw, 2 }, { 0, 0, 0, 0 }, {
				3, straight, ccw, 4 }, { 3, straight, ccw, 4 }, { 3, straight,
				ccw, 4 }, { 3, straight, ccw, 4 }, { 3, thrown, cw, 9 }, { 3,
				thrown, cw, 9 }, { 3, thrown, cw, 9 }, { 3, straight, ccw, 4 },
				{ 3, straight, ccw, 4 }, { 3, straight, ccw, 4 }, { 3, straight,
						ccw, 4 }, { 3, straight, ccw, 4 },
				{ 3, straight, ccw, 4 } },

		/*4*/{ { 4, straight, cw, 3 }, { 4, straight, cw, 3 }, { 4, straight,
				cw, 3 }, { 0, 0, 0, 0 }, { 5, straight, ccw, 5 }, { 5, straight,
				ccw, 5 }, { 5, straight, ccw, 5 }, { 4, straight, cw, 3 }, { 4,
				straight, cw, 3 }, { 4, straight, cw, 3 },
				{ 5, straight, ccw, 5 }, { 5, straight, ccw, 5 }, { 4, thrown,
						cw, 3 }, { 5, straight, ccw, 5 },
				{ 5, straight, ccw, 5 }, { 5, thrown, ccw, 16 } },

		/*5*/{ { 6, straight, ccw, 6 }, { 6, straight, ccw, 6 }, { 5, straight,
				cw, 4 }, { 5, straight, cw, 4 }, { 0, 0, 0, 0 }, { 6, straight,
				ccw, 6 }, { 6, thrown, ccw, 12 }, { 5, straight, cw, 4 }, { 5,
				straight, cw, 4 }, { 5, straight, cw, 4 },
				{ 6, thrown, ccw, 12 }, { 6, thrown, ccw, 12 }, { 5, straight,
						cw, 4 }, { 6, straight, ccw, 6 },
				{ 6, thrown, ccw, 12 }, { 5, straight, cw, 4 } },

		/*6*/{ { none, none, ccw, 1 }, { none, none, ccw, 1 }, { none, none,
				ccw, 1 }, { none, none, cw, 5 }, { none, none, cw, 5 }, { 0, 0,
				0, 0 }, { none, none, ccw, 1 }, { none, none, cw, 5 }, { none,
				none, cw, 5 }, { none, none, cw, 5 }, { none, none, ccw, 1 }, {
				none, none, ccw, 1 }, { none, none, ccw, 1 }, { none, none, ccw,
				1 }, { none, none, ccw, 1 }, { none, none, cw, 5 } },

		/*7*/{ { 9, straight, cw, 12 }, { none, none, ccw, 8 }, { none, none,
				ccw, 8 }, { none, none, ccw, 8 }, { 9, straight, cw, 12 }, { 9,
				straight, cw, 12 }, { 0, 0, 0, 0 }, { none, none, ccw, 8 }, {
				none, none, ccw, 8 }, { none, none, ccw, 8 }, { 9, straight, cw,
				12 }, { 9, straight, cw, 12 }, { none, none, ccw, 8 }, { 9,
				straight, cw, 12 }, { 9, thrown, cw, 12 },
				{ 9, straight, cw, 12 } },

		/*8*/{ { none, none, cw, 7 }, { none, none, ccw, 9 }, { none, none, ccw,
				9 }, { none, none, ccw, 9 }, { none, none, cw, 7 }, { none,
				none, cw, 7 }, { none, none, cw, 7 }, { 0, 0, 0, 0 }, { none,
				none, ccw, 9 }, { none, none, ccw, 9 }, { none, none, ccw, 9 },
				{ none, none, cw, 7 }, { none, none, ccw, 9 }, { none, none, cw,
						7 }, { none, none, cw, 7 }, { none, none, cw, 7 } },

				/*9*/{ { 7, straight, cw, 8 }, { 7, thrown, ccw, 3 }, { 7,
						thrown, ccw, 3 }, { 7, thrown, ccw, 3 }, { 7, straight,
						cw, 8 }, { 7, straight, cw, 8 }, { 7, straight, cw, 8 },
						{ 7, straight, cw, 8 }, { 0, 0, 0, 0 }, { 7, straight,
								ccw, 10 }, { 7, straight, ccw, 10 }, { 7,
								straight, cw, 8 }, { 7, thrown, ccw, 3 }, { 7,
								straight, cw, 8 }, { 7, straight, cw, 8 }, { 7,
								straight, cw, 8 } },

				/*10*/{ { none, none, cw, 9 }, { none, none, ccw, 11 }, { none,
						none, ccw, 11 }, { none, none, ccw, 11 }, { none, none,
						cw, 9 }, { none, none, cw, 9 }, { none, none, cw, 9 }, {
						none, none, cw, 9 }, { none, none, cw, 9 },
						{ 0, 0, 0, 0 }, { none, none, ccw, 11 }, { none, none,
								ccw, 11 }, { none, none, ccw, 11 }, { none,
								none, cw, 9 }, { none, none, cw, 9 }, { none,
								none, cw, 9 } },

				/*11*/{ { none, none, cw, 10 }, { none, none, ccw, 12 }, { none,
						none, ccw, 12 }, { none, none, ccw, 12 }, { none, none,
						cw, 10 }, { none, none, cw, 10 },
						{ none, none, ccw, 12 }, { none, none, ccw, 12 }, {
								none, none, cw, 10 }, { none, none, cw, 10 }, {
								0, 0, 0, 0 }, { none, none, ccw, 12 }, { none,
								none, ccw, 12 }, { none, none, cw, 10 }, { none,
								none, ccw, 12 }, { none, none, cw, 10 } },

				/*12*/{ { 8, thrown, cw, 5 }, { 8, straight, ccw, 7 }, { 8,
						straight, ccw, 7 }, { 8, straight, ccw, 7 }, { 8,
						thrown, cw, 5 }, { 8, thrown, cw, 5 }, { 8, straight,
						ccw, 7 }, { 8, straight, ccw, 7 },
						{ 8, straight, ccw, 7 }, { 8, straight, cw, 11 }, { 8,
								straight, cw, 11 }, { 0, 0, 0, 0 }, { 8,
								straight, ccw, 7 }, { 8, thrown, cw, 5 }, { 8,
								straight, ccw, 7 }, { 8, thrown, cw, 5 } },

				/*13*/{ { 1, thrown, cw, 1 }, { 1, thrown, cw, 1 }, { 4, thrown,
						ccw, 4 }, { 4, thrown, ccw, 4 }, { 4, thrown, ccw, 4 },
						{ 1, thrown, cw, 1 }, { 4, thrown, ccw, 4 }, { 4,
								thrown, ccw, 4 }, { 4, thrown, ccw, 4 }, { 4,
								thrown, ccw, 4 }, { 4, thrown, ccw, 4 }, { 4,
								thrown, ccw, 4 }, { 0, 0, 0, 0 }, { 1, thrown,
								cw, 1 }, { 4, thrown, ccw, 4 }, { 4, thrown,
								ccw, 4 } },

				/*14*/{ { 2, thrown, cw, 2 }, { 2, thrown, cw, 2 }, { 2, thrown,
						cw, 2 }, { 2, thrown, cw, 2 }, { 2, thrown, cw, 2 }, {
						2, thrown, cw, 2 }, { 2, thrown, cw, 2 }, { 2, thrown,
						cw, 2 }, { 2, thrown, cw, 2 }, { 2, thrown, cw, 2 }, {
						2, thrown, cw, 2 }, { 2, thrown, cw, 2 }, { 2, thrown,
						cw, 2 }, { 0, 0, 0, 0 }, { 2, thrown, cw, 2 }, { 2,
						thrown, cw, 2 } },

				/*15*/{ { 9, thrown, ccw, 7 }, { 9, thrown, ccw, 7 }, { 9,
						thrown, ccw, 7 }, { 9, thrown, ccw, 7 }, { 9, thrown,
						ccw, 7 }, { 9, thrown, ccw, 7 }, { 9, thrown, ccw, 7 },
						{ 9, thrown, ccw, 7 }, { 9, thrown, ccw, 7 }, { 9,
								thrown, ccw, 7 }, { 9, thrown, ccw, 7 }, { 9,
								thrown, ccw, 7 }, { 9, thrown, ccw, 7 }, { 9,
								thrown, ccw, 7 }, { 0, 0, 0, 0 }, { 9, thrown,
								ccw, 7 } },

				/*16*/{ { 5, thrown, cw, 4 }, { 5, thrown, cw, 4 }, { 5, thrown,
						cw, 4 }, { 5, thrown, cw, 4 }, { 5, thrown, cw, 4 }, {
						5, thrown, cw, 4 }, { 5, thrown, cw, 4 }, { 5, thrown,
						cw, 4 }, { 5, thrown, cw, 4 }, { 5, thrown, cw, 4 }, {
						5, thrown, cw, 4 }, { 5, thrown, cw, 4 }, { 5, thrown,
						cw, 4 }, { 5, thrown, cw, 4 }, { 5, thrown, cw, 4 }, {
						0, 0, 0, 0 } }, };

unsigned char switch_state[total_switches + 1] = { none, straight, straight,
		straight, straight, straight, straight, straight, straight, straight };

enum {
	TEST, PON, POFF, DONE
};

int HS1cw[] = { PON, 6, DONE };
int HS1ccw[] = { DONE };
int HS2cw[] = { DONE };
int HS2ccw[] = { PON, 2, TEST, 1, 6, DONE, PON, 13, DONE };
int HS3cw[] = { PON, 1, DONE };
int HS3ccw[] = { TEST, 2, 4, DONE, PON, 14, DONE };
int HS4cw[] = { DONE };
int HS4ccw[] = { PON, 3, DONE };
int HS5cw[] = { PON, 2, DONE };
int HS5ccw[] = { DONE };
int HS6cw[] = { TEST, 3, 4, DONE, PON, 9, DONE };
int HS6ccw[] = { PON, 4, DONE };
int HS7cw[] = { PON, 3, TEST, 4, 6, DONE, PON, 13, DONE };
int HS7ccw[] = { DONE };
int HS8cw[] = { DONE };
int HS8ccw[] = { TEST, 5, 6, PON, 5, DONE, PON, 16, DONE };
int HS9cw[] = { PON, 4, DONE };
int HS9ccw[] = { TEST, 6, 4, DONE, PON, 12, DONE };
int HS10cw[] = { DONE };
int HS10ccw[] = { PON, 6, DONE };
int HS11cw[] = { PON, 5, DONE };
int HS11ccw[] = { DONE };
int HS12cw[] = { DONE };
int HS12ccw[] = { PON, 1, DONE };
int HS13cw[] = { PON, 12, TEST, 9, 6, DONE, PON, 15, DONE };
int HS13ccw[] = { DONE };
int HS14cw[] = { DONE };
int HS14ccw[] = { PON, 8, DONE };
int HS15cw[] = { PON, 7, DONE };
int HS15ccw[] = { DONE };
int HS16cw[] = { DONE };
int HS16ccw[] = { PON, 9, DONE };
int HS17cw[] = { PON, 8, DONE };
int HS17ccw[] = { TEST, 7, 4, DONE, PON, 3, DONE };
int HS18cw[] = { DONE };
int HS18ccw[] = { PON, 10, DONE };
int HS19cw[] = { PON, 9, DONE };
int HS19ccw[] = { DONE };
int HS20cw[] = { PON, 11, DONE };
int HS20ccw[] = { DONE };
int HS21cw[] = { PON, 10, DONE };
int HS21ccw[] = { DONE };
int HS22cw[] = { DONE };
int HS22ccw[] = { PON, 12, DONE };
int HS23cw[] = { PON, 11, DONE };
int HS23ccw[] = { DONE };
int HS24cw[] = { DONE };
int HS24ccw[] = { PON, 7, DONE };
int HS25cw[] = { TEST, 1, 4, DONE, PON, 2, PON, 1, DONE };
int HS25ccw[] = { POFF,2,DONE };//previous section to move function
int HS26cw[] = { POFF,3,DONE};//is 1 and 4 since hall sensors in sections 2 and 3 not crossed
int HS26ccw[] = { TEST, 4, 4, DONE, PON, 3, PON, 4, DONE };
int HS27cw[] = { TEST, 2, 4, DONE, PON, 2 };
int HS27ccw[] = { DONE };
int HS28cw[] = { DONE };
int HS28ccw[] = { POFF, 14, DONE };
int HS29cw[] = { DONE };
int HS29ccw[] = { TEST, 9, 4, DONE, PON, 9, PON, 7 };
int HS30cw[] = { POFF, 15, DONE };
int HS30ccw[] = { DONE };
int HS31cw[] = { PON, 4, TEST, 5, 7, POFF, 4, DONE };
int HS31ccw[] = { DONE };
int HS32cw[] = { DONE };
int HS32ccw[] = { POFF, 16, DONE };
//                0     1        2     3      4        5      6     7
int *halls[] = { NULL, HS1cw, HS1ccw, HS2cw, HS2ccw, HS3cw, HS3ccw, HS4cw,
		HS4ccw, HS5cw, HS5ccw, HS6cw, HS6ccw, HS7cw, HS7ccw, HS8cw, HS8ccw, //16
		HS9cw, HS9ccw, HS10cw, HS10ccw, HS11cw, HS11ccw, HS12cw, HS12ccw, //24
		HS13cw, HS13ccw, HS14cw, HS14ccw, HS15cw, HS15ccw, HS16cw, //31
		HS16ccw, HS17cw, HS17ccw, HS18cw, HS18ccw, HS19cw, HS19ccw, //38
		HS20cw, HS20ccw, HS21cw, HS21ccw, HS22cw, HS22ccw, HS23cw, HS23ccw, //46
		HS24cw, HS24ccw, HS25cw, HS25ccw, HS26cw, HS26ccw, HS27cw, HS27ccw, //54
		HS28cw, HS28ccw, HS29cw, HS29ccw, HS30cw, HS30ccw, HS31cw, HS31ccw, //62
		HS32cw, HS32ccw }; //64
//ccw=hs*2, cw=hs*2-1

int section[total_HS + 1] = { 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8,
		9, 9, 10, 10, 11, 11, 12, 12, 13, 13, 14, 14, 15, 15, 16, 16 };
//void pon(unsigned char segid, char v);
//void poff(unsigned char segid);
//void poffall();
//void SwitchToState(unsigned char switchid, char state);

/* TRACKACTION_PORT expects data to be a pointer to a struct of type 'trackstate' (found in trainmessagestructs.h)
 * trackstate
 * {
 * type = {SWITCH OR TRACK}
 * id = id of the switch or track of interest. 1-9 or 1-16
 * arg = the new state. +7 to -7 for track (with +ve values being clockwise and -ve values being cclockwise)
 * OR
 * arg = the new state. 0 is thrown, 1 is straight
 * }
 * reminder: enum {HALL = 0, SWITCH = 1, TRACK = 2}*/
#define delay 300000

/*******************************************
 * MOVEMENT FUNCTIONS
 ******************************************/
unsigned char move(unsigned char current, unsigned char target,
		unsigned int* portlist, unsigned train_number) {
	/*	for use in action table subtract 1 from current and target.
	 * Movement is done in two ways, the first via this function and then
	 * the virtual machine. This function decides the direction based on
	 * the routing table and sets the initial section to a standard speed.
	 * This function also switches the switches based on the section and
	 * target.
	 */

	tmsg controller_message;
	controller_message.HS=train_number;
	controller_message.start_section=current;
	controller_message.destination_section=action[current-1][target-1].next;
	sendmsg((char*)&controller_message,TRAIN_LOCATIONS,sizeof(tmsg));

	unsigned char prev_section = none;
	char speed = std_speed;
	if (action[current - 1][target - 1].dir == ccw)
		speed = -speed;
	unsigned char current_temp = none;
	trackstate state;
	state.type = TRACK;
	state.id = current;
	state.arg = speed;
	int returnmessage = sendmsg((char*) &state, TRACKACTION_PORT,
			sizeof(trackstate));
	if(returnmessage!=MSG_SUCCESS)
	{
		outputstr("FAILED TO START SECTION\0");
		return 1;
	}
	else
		outputstr("STARTED INITIAL SECTION\0");

	//pon(current,speed);
	int i=0;
	char * msgptr;
	while (current != target) {

		if(train_number==train_1)
		{
			msgptr=recvmsg(TRAIN_1_COL);
			if(msgptr!=NO_MSG)
				return 0xc;//collision iminant!
		}
		else
		{
			msgptr=recvmsg(TRAIN_2_COL);
			if(msgptr!=NO_MSG)
				return 0xc;//collision iminant!
		}
		if (action[current - 1][target - 1].sw!=none) {
			state.type = SWITCH;
			state.id = action[current - 1][target - 1].sw;
			state.arg = action[current - 1][target - 1].sw_action;
			returnmessage = sendmsg((char*) &state, TRACKACTION_PORT,
					sizeof(trackstate));
			if(returnmessage!=MSG_SUCCESS)
			{
				outputstr("FAILED TO SIGNAL SWITCH\0");
				return 1;
			}
//			if(action[action[current-1][target-1].next][target-1].sw!=none)
//			{
//				state.id=action[action[current-1][target-1].next][target-1].sw;
//				state.arg=action[action[current-1][target-1].next][target-1].sw_action;
//				returnmessage = sendmsg((char*) &state, TRACKACTION_PORT,
//						sizeof(trackstate));
//				if(returnmessage!=MSG_SUCCESS)
//				{
//					outputstr("FAILED TO SIGNAL SWITCH\0");
//					return 1;
//				}
//			}
			//SwitchToState(action[current][target].sw,action[current][target].sw_action);
			//switch_state[action[current-1][target-1].sw]=action[current-1][target-1].sw_action;
			//put in switching function
		}


		current_temp = virtual_machine(action[current - 1][target - 1].dir,
				portlist);

		if (current_temp != current) {
			prev_section = current;
			state.type = TRACK;
			state.id = prev_section;
			state.arg = 0;
			returnmessage = sendmsg((char*) &state, TRACKACTION_PORT,
					sizeof(trackstate));
			if(returnmessage!=MSG_SUCCESS)
			{
				outputstr("FAILED TO TURN OFF SECTION\0");
				return 1;
			}
			//poff(prev_section)

			current = current_temp;
			controller_message.start_section=current;
			controller_message.destination_section=action[current-1][target-1].next;
			sendmsg((char*)&controller_message,TRAIN_LOCATIONS,sizeof(tmsg));
		}

		if (target == 13 || target == 16 || target == 14
				|| target == 15 && current != target) {
			while(i<delay)
					i++;
			i=0;
			outputstr("Special case \0");
//			state.type = SWITCH;
//			state.id = action[current - 1][target - 1].sw;
//			state.arg = action[current - 1][target - 1].sw_action;
//			returnmessage = sendmsg((char*) &state, TRACKACTION_PORT,
//					sizeof(trackstate));
//			//special cases not on oval track - switches direction
//			state.type = TRACK;
//			state.id = current;
//			state.arg = 0;
//			returnmessage = sendmsg((char*) &state, TRACKACTION_PORT,
//								sizeof(trackstate));
//			while(i<delay)
//				i++;
//			i=0;
//			speed = std_speed;
//			if (action[current - 1][target - 1].dir == ccw)
//				speed = -speed;
//			state.arg = speed;
//			returnmessage = sendmsg((char*) &state, TRACKACTION_PORT,
//					sizeof(trackstate));
		}

	}
	while(i<delay)
		i++;

	state.type = TRACK;
	state.id = current;
	state.arg = 0;
	returnmessage = sendmsg((char*) &state, TRACKACTION_PORT,
			sizeof(trackstate));
	//poff(current)
	outputstr("DONE MOVE\0");
	return current;
}

unsigned char virtual_machine(unsigned dir, unsigned int* portlist) {
	unsigned hs = get_hs(portlist);
	outputstr("Done POLING HS");
	unsigned char current = (unsigned char) section[hs];
	hs = hs * 2;		//for indexing in halls[hs][pc]
	if (dir == cw)
		hs -= 1;		//ccw=hs*2, cw=hs*2-1
	unsigned pc = 0;		//indexing in halls[hs][pc]
	char speed = std_speed;
	if (dir == ccw)
		speed = -speed;
	unsigned char section = none;
	int sw = none;
	trackstate state;
	int returnmessage = 0;
	while (halls[hs][pc] != DONE) {
		switch (halls[hs][pc]) {
		case TEST:
			sw = halls[hs][++pc];
			if (switch_state[sw] == thrown)
				pc = halls[hs][++pc];
			else
				pc+=2;
			break;
		case PON:
			section = (unsigned char) halls[hs][++pc];
			state.type = TRACK;
			state.id = section;
			state.arg = speed;
			returnmessage = sendmsg((char*) &state, TRACKACTION_PORT,
					sizeof(trackstate));
			if(returnmessage!=MSG_SUCCESS)
			{
				outputstr("FAILED TO START SECTION\0");
				return 1;
			}
			//pon(section,speed);
			pc++;
			break;
		case POFF:
			section = (unsigned char) halls[hs][++pc];
			state.type = TRACK;
			state.id = section;
			state.arg = 0;
			returnmessage = sendmsg((char*) &state, TRACKACTION_PORT,
					sizeof(trackstate));
			if(returnmessage!=MSG_SUCCESS)
			{
				outputstr("FAILED TO TURN OFF SECTION\0");
				return 1;
			}
			//poff(section);
			pc++;
			break;
		}
		//pc++;
	}

		return current;

}

unsigned get_hs(unsigned int* portlist) {
	char *hs;
	outputstr("POLING HS");
	poll(portlist, 1);
	//now signalled
	hs = recvmsg(*portlist);
	if (hs != NO_MSG)
		return (unsigned) *hs;
	else
		return 1; // error

}
/***********************************
 * TRAIN PROCESSES
 ***********************************/
void train1() {
	unsigned train_number = train_1;
	int returnmessage = 0;
	tmsg* msgdata;
	bind(TRAIN1_HS);
	bind(TR1_CTRL);
	bind(TRAIN_1_COL);
	unsigned int portlist[] = { TR1_CTRL };
	unsigned int passed_portlist[] = { TRAIN1_HS };
	while (1) {
		poll(portlist, 1);
		msgdata = (tmsg*) recvmsg(*portlist);
		if (msgdata != NO_MSG)
			move(msgdata->start_section, msgdata->destination_section,
					passed_portlist,train_number);
		returnmessage = sendmsg(NULL, TRAIN_1_IDLE,
							sizeof(char*));
	}/* while(1) */
}

void train2() {
	/*
	 * Process that
	 */
	unsigned train_number = train_2;
	bind(TR2_CTRL);
	bind(TRAIN2_HS);
	bind(TRAIN_2_COL);
	tmsg* msgdata;
	int returnmessage = 0;
	unsigned int portlist[] = { TR2_CTRL };
	unsigned int passed_portlist[] = { TRAIN2_HS };
	while (1) {
		poll(portlist, 1);
		msgdata = (tmsg*) recvmsg(*portlist);
		move(msgdata->start_section, msgdata->destination_section,
					passed_portlist,train_number);

		returnmessage = sendmsg(NULL, TRAIN_2_IDLE,
							sizeof(char*));
	}/* while(1) */

}

struct controller_train_state{
	unsigned prev;
	unsigned current;
	unsigned next;
};

struct controller_train_state trains[2];
/***********************************************
 * CONTROL PROCESSES
 ***********************************************/
void HS_handler(){

	/*
	 * Process runs when HS information is received, part of the controlling
	 * processes.
	 * This process takes retrieves the information and determines which train to
	 * forward the data on to based the state saved.
	 * a function that checks to see if the trains are too close to
	 * each other is called
	 */
	//sendmsg((char*)&HS,TRAIN1_HS,sizeof(char*));
	bind(HS_DATA);
	tmsg *data;
	unsigned int portlist=HS_DATA;
	unsigned char HS;//= data.HS;
	unsigned char handler_section;// = section[HS];
	while(1)
	{
	poll(&portlist,1);
	data=(tmsg*)recvmsg(portlist);
	HS = data->HS;
	handler_section = section[HS];

	if(handler_section==trains[train_1].current||handler_section==trains[train_1].next)
		sendmsg((char*)&HS,TRAIN1_HS,sizeof(char*));
	else if(handler_section==trains[train_2].current||handler_section==trains[train_2].next)
		sendmsg((char*)&HS,TRAIN2_HS,sizeof(char*));

	}

	//check_collision();

}
//void (*controller_functions[])(tmsg *data)=
//{control_T1,control_T2,HS_handler,update_state};

void control_T2(){
	/*
	 * Forwards on the initial message for train 2 and sets initial state
	 * changes the destination when the train 2 process indicates
	 * that is is idle with a message.
	 */
	bind(TRAIN2_CMDS);
	bind(TRAIN_2_IDLE);
	char *msgdata;
	unsigned int portlist1[]={TRAIN_2_IDLE};
	unsigned int portlist2[]={TRAIN2_CMDS};
	unsigned char temp = 0;
	unsigned char current;// = data->start_section;
	unsigned char target;// = data->destination_section;
	//initial state
	tmsg T2_MESSAGE;
	tmsg *data;
	poll(portlist2,1);
	data=(tmsg*)recvmsg(*portlist2);
	//getting initial commands
	current = data->start_section;
	target = data->destination_section;
	trains[train_2].current=current;
	trains[train_2].next=action[current-1][target-1].next;
	while(1)
	{

		T2_MESSAGE.start_section=current;
		T2_MESSAGE.destination_section=target;

		sendmsg((char*) &T2_MESSAGE, TR2_CTRL,
				sizeof(tmsg));

		poll(portlist1,1);
		//idle port
		msgdata=recvmsg(*portlist1);
		//DO NEED!

		T2_MESSAGE.start_section = target;
		T2_MESSAGE.destination_section = current;
				// go backwards and forwards
		temp = current;
		current = target;
		target = temp;
	}
}
void control_T1(){
	/*
	 * Forwards on the initial message for train 1 and sets initial state
	 * changes the destination when the train 1 process indicates
	 * that is is idle with a message.
	 */
	bind(TRAIN1_CMDS);
	bind(TRAIN_1_IDLE);
	char *msgdata;
	unsigned int portlist1[]={TRAIN_1_IDLE};
	unsigned int portlist2[]={TRAIN1_CMDS};
	unsigned char temp = 0;
	unsigned char current;// = data->start_section;
	unsigned char target;// = data->destination_section;
	//initial state
	tmsg T1_MESSAGE;
	tmsg *data;
	poll(portlist2,1);
	data=(tmsg*)recvmsg(*portlist2);
	//getting initial commands
	current = data->start_section;
	target = data->destination_section;
	trains[train_1].current=current;
	trains[train_1].next=action[current-1][target-1].next;
	while(1)
	{
		T1_MESSAGE.start_section=current;
		T1_MESSAGE.destination_section=target;

		sendmsg((char*) &T1_MESSAGE, TR1_CTRL,
				sizeof(tmsg));
		poll(portlist1,1);
		//waiting for idle message
		msgdata=recvmsg(*portlist2);
		//garbage, the message being there is good enough

		T1_MESSAGE.start_section = target;
		T1_MESSAGE.destination_section = current;
		// go backwards and forwards
		temp = current;
		current = target;
		target = temp;
	}
}

void update_state(){
	//using tmsg for HS_HANDLER:
	//tmsg.start_section = current section
	//tmsg.desination_section = next section
	//tmsg.hs = train number
	/*
	 * updates the state as provided by the messages received from
	 * train processes.
	 */
	bind(TRAIN_LOCATIONS);
	tmsg *data;
	unsigned int portlist = TRAIN_LOCATIONS;
	while(1)
	{
		poll(&portlist,1);
		data=(tmsg*)recvmsg(portlist);

	trains[data->HS].prev=trains[data->HS].current;
	trains[data->HS].current=data->start_section;
	trains[data->HS].next=data->destination_section;
	}
}

void check_collision(){
	int returnmessage = 0;
	trackstate state;
	if(trains[train_1].current==trains[train_2].next)
	{
		//train 2 will collide with 1. turn off trains[train_2].current
		state.type = TRACK;
		state.id = trains[train_2].current;
		state.arg = 0;
		returnmessage = sendmsg((char*) &state, TRACKACTION_PORT,
					sizeof(trackstate));
		if(returnmessage!=MSG_SUCCESS)
		{
			outputstr("FAILED TO START SECTION\0");
		}
		returnmessage = sendmsg(NULL,TRAIN_2_COL,sizeof(char*));
		//send message to train 2 to end the move function.

	}
	else if(trains[train_2].current==trains[train_1].next)
	{
		//train 1 will collide with 2. turn off trains[train_2].current
		state.type = TRACK;
		state.id = trains[train_1].current;
		state.arg = 0;
		returnmessage = sendmsg((char*) &state, TRACKACTION_PORT,
					sizeof(trackstate));
		if(returnmessage!=MSG_SUCCESS)
		{
			outputstr("FAILED TO START SECTION\0");
		}
		returnmessage = sendmsg(NULL,TRAIN_1_COL,sizeof(char*));
		//send message to train 1 to turn off
	}
}
