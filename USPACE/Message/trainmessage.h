#ifndef EXAMPLEFUNCTIONS_H_
#define EXAMPLEFUNCTIONS_H_

#include "../Message/trainmessageports.h"
#include "../Message/trainmessagestructs.h"
#include "../../MOS/kernel/message.h"
#include "../../MOS/devices/mem.h"
/*
* message.h
*
* Created on: Nov 13, 2014
* Author: matt
*/

#define NO_MSG 0
#define EMPTY_MSG 0

char* recvmsg(unsigned int);
msgReturnCode sendmsg(char*, unsigned int, unsigned int);
msgReturnCode hsendmsg(char*, unsigned int, unsigned int);
/*
* Sender:
*
* while(1)
* {
* mymsg = (msg *)allocate(sizof(msg))
* unsigned int dispPort = DISPLAY_PORT
* msg.data = &mydata
* if (send(mymsg, dispPort) == MSG_SUCCESS)
* /// get new data
*
* poll(&dispPort,1) // wait until the display process reads something
*
*
*
* }
*
* receiver:
* unsigned int dispPort = DISPLAY_PORT
*
* if bind(dispPort)
* while(1)
* {
* mymsg = (msg *)allocate(sizof(msg))
*
* if recv(mymsg,dispPort)
* do stuff
*
* }
*
*
*
*/

/* HOW THIS WORKS: Memory
 * SEND
 * I allocate memory for the message on the send.
 * Then I delete the message after the send is complete
 *
 * RECEIVE
 * I do not allocate memory for the message.
 * I delete the message once I am done with it.
 *
 * FORWARDING
 * I do not allocate memory for the message.
 * I delete the message once it is forwarded.
 */

#endif /* EXAMPLEFUNCTIONS_H_ */
