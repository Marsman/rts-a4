/*
 * MessageStructs.h
 *
 *  Created on: 2014-11-15
 *      Author: William
 */

#ifndef MESSAGESTRUCTS_H_
#define MESSAGESTRUCTS_H_

#include "../GUI/gui.h"

/*********** OS ************/
// TODO: remove processarea and name from newguimsg and reference these parameters from the gui* directly. This will require code changes in a couple spots so I've left it for now. low priority
typedef struct newguimsg {
	gui* newGUI;
	square processarea;
	char* name;
}newguimsg;

/* switch active typing process to this process */
typedef struct tswitchmsg {
	unsigned int portid;
}tswitchmsg;

/* print requests */
typedef struct printmsg {
	gui* targetGUI;
	char* str;
	cursor_pos print_pos;
}printmsg;

/* UART receive message */
typedef struct U0recvmsg {
	char chr;
}U0rm;

/************ MAP ************/
enum {HALL = 0, TRACK = 1, SWITCH = 2}; // msg_type parameter
typedef struct updatesymbolmsg {
	char msg_type;
	char train;
	char sym_id;
	char state;
	signed char speed;				// only used for tracks
}usmsg;

/************ ATmel controller *************/
/* hall sensor has activated message */
#define ATMEL1 0x01
#define ATMEL2 0x02
#define ATMEL3 0x03
typedef struct halltrigger {
	unsigned char atmel;
}halltrigger;

/* adjusts the switch or track functionality */
typedef struct testmsg {
	char type;
	unsigned char id;
	unsigned char arg;
}trackstate;

/************ TRAIN SHIT *******************/
struct trainmessage{
	unsigned HS;
	unsigned start_section;
	unsigned destination_section;
	unsigned speed;
};

typedef struct trainmessage tmsg;

#endif /* MESSAGESTRUCTS_H_ */
