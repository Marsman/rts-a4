/*
 * MessagePorts.h
 *
 *  Created on: 2014-11-15
 *      Author: William
 *
 * A collection of ports used by various processes throughout the program for communication
 */

#ifndef MESSAGEPORTS_H_
#define MESSAGEPORTS_H_

/* no port */
#define NULL_PORT 				0

/* ports */
#define TRAINCONTROLLER_PORT 	1
#define MAP_PORT 				2
#define NEWUI_PORT 				3
#define CONSOLE_PORT 			4
#define SWITCHACTIVETYPING_PORT 5
#define UART0RECV_PORT 			6
#define PRINT_PORT				7
#define TRACKACTION_PORT		8
#define OUTPUT_PORT				17
#define PORTF_PORT 				9
#define CONTEXTSWITCH_PORT 		10
#define DISPLAY		 			11
#define KEYBOARD				12

/* train specific ports */
#define TRAIN1_HS   13
#define TRAIN2_HS	18
#define TRAIN1_CMDS 14
#define TRAIN2_CMDS 15
#define TR2_CTRL    16
#define TR1_CTRL	19
#define HS_DATA		20
#define TRAIN_LOCATIONS 21
#define TRAIN_1_IDLE 22
#define TRAIN_2_IDLE 23
#define TRAIN_1_COL	 24
#define TRAIN_2_COL  25

#endif /* MESSAGEPORTS_H_ */
