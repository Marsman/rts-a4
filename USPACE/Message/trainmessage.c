/*
 * trainmessage.c
 *
 *  Created on: 2014-11-20
 *      Author: William
 */

#include "trainmessage.h"
#include "../../MOS/kernel/hmessage.h"

void msg_fail(){
	/* called exclusively by sendmsg when/if we run out of memory */
	while(1);
}

#define ALLOC_FAILED 0
msgReturnCode sendmsg(char* data, unsigned int destPort, unsigned int datasz){
/* an extension of message passing to make it easier. Frees data when message is sent */
msg bmsg;
msgReturnCode rtn;
bmsg.data = data;
bmsg.size = datasz;
rtn = send(&bmsg, destPort);
return rtn;
}

#define NO_MSG 0
char* recvmsg(unsigned int recvPort){
/* returns the data pointer only, stripping the message container */
msg* rmsg;
char* rtndata;
if(recv(&rmsg, recvPort) == MSG_SUCCESS){
	rtndata = rmsg->data;
	return rtndata; /* return data value is a pointer to a memory block */
	/* when the data is deallocated, the message will also be deallocated */
} else {
	return NO_MSG;
} /* nothing to receive, return no message instead of pointer */
}

msgReturnCode hsendmsg(char* data, unsigned int destPort, unsigned int datasz){
	msg bmsg;
	msgReturnCode rtn;
	bmsg.data = data;
	bmsg.size = datasz;
	rtn = hsend(&bmsg, destPort);
	return rtn;
}
