/*
 * ScreenManager.h
 *
 *  Created on: 2014-11-14
 *      Author: William
 */

#ifndef SCREENMANAGER_H_
#define SCREENMANAGER_H_

#include "../Message/trainmessage.h"
#include "../GUI/gui.h"

#define NULL_PORT 0
#define PORTLIST_LEN 4
static const unsigned int portlist[]= {
		NEWUI_PORT, 				// initialize a GUI for a process
		PRINT_PORT,					// print request
		SWITCHACTIVETYPING_PORT,	// change typing GUI
		KEYBOARD					// forward to currently active typing GUI
};

void UART0recv_message(char*);
void switchtyping_message(char*);
void print_message(char*);
void newui_message(char*);

void (*msghandler[])(char *)={
		newui_message,
		print_message,
		switchtyping_message,
		UART0recv_message
};

void MainScreenProcess();

#endif /* SCREENMANAGER_H_ */
