/*
 * InterfaceManager.c
 *
 *  Created on: 2014-11-14
 *      Author: William
 *
 *	Used to manage each processes screen functions.
 *	Initialises GUI for requesting processes and handles all writing to the screen for other processes.
 *
 *	Q: How to store the processes inside this structure ???
 *	M1: in a linked list (must be searched)
 *	M2: in a statically sized array (limiting number of processes)
 *
 *	I'll go with the array method for now.
 *
 *	all functions and methods done
 *
 *	still need to integrate with matts better: see TODO for details.
 */

#include "screenmanager.h"

void newui_message(char*);
void print_message(char*);
void switchtyping_message(char*);
void UART0recv_message(char*);

unsigned int UART0_fwd_port = NULL_PORT;

#define GLOBAL_SPACE {{0,0,9999,9999}, {{0,0,9999,9999}, {0, 0}}}
gui globalgui = GLOBAL_SPACE;
void MainScreenProcess(){
/* This process waits for messages and does stuff based on where the message is coming from.
 * CASES :
 * Message is coming on CREATE GUI FOR PROCESS PORT (done in user space)
 * Message is coming on PRINT STRING PORT (from running process)
 * Message is coming on UART0 recv port (from UART0)
 * Message is coming on SWITCH TYPING PROCESS PORT
 */
	char* msgdata;
	int i;
	bind(NEWUI_PORT);
	bind(PRINT_PORT);
	bind(SWITCHACTIVETYPING_PORT);
	bind(KEYBOARD);
	/* erase anything currently on the terminal */
	ClearScreen();

	/* initialize this process for messaging */
	while(1){
		poll((unsigned int*) portlist, PORTLIST_LEN);
		for(i = 0; i < PORTLIST_LEN; i++){
			msgdata = recvmsg(portlist[i]);
			if(msgdata != NO_MSG){
				msghandler[i](msgdata);
				del((msg*)msgdata);
				/* message data must be deallocated once done with it */
			}
		}
	}/* while(1) */
}

void newui_message(char* ui_msg){
/* initialize a ui given the following message */
	newguimsg* newgui = (newguimsg*) ui_msg;
	ChangeProcessGUI(&globalgui);
	InitProcessGUI(newgui->newGUI, newgui->processarea, newgui->name);
}

void print_message(char* print_msg){
/* map to print message, change the gui if necessary and print to the screen */
	printmsg* print = (printmsg*) print_msg;
	ChangeProcessGUI(print->targetGUI);
	PrintCoord(print->print_pos, print->str);
}

void switchtyping_message(char* switch_msg){
/* switch the typing process port to this value. This is where UART0 characters will be forwarded */
	tswitchmsg* newtyping = (tswitchmsg*) switch_msg;
	UART0_fwd_port = newtyping->portid;
}

void UART0recv_message(char* UART0_msg){
/* forward the currently set forward port if enabled */
	msg U0_fwd;
	U0_fwd.data = UART0_msg;
	U0_fwd.size = sizeof(U0rm);
	send(&U0_fwd, UART0_fwd_port);
}
