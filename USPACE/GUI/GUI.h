/*
 * GUI.h
 *
 *  Created on: 2014-11-14
 *      Author: William
 */

#ifndef GUI_H_
#define GUI_H_

#define GLOBAL true
#define LOCAL false
#define WRITEONLY 1
#define STREND '\0'
#define RETURNSTR "\r"
#define LINESTR "\n"
#define __RETURNCHR 13
#define __BS 127
#define __ESC 27
#define __BACKSLASH 223
#define __INS_BACKSLASH 92
#define __SPACE " "
#define __CHR_SPACE 32
#define bool int
#define true 1
#define false 0

typedef struct cursor_pos{
	unsigned int x;
	unsigned int y;
}cursor_pos;

typedef struct square{
	unsigned int top;
	unsigned int left;
	unsigned int width;
	unsigned int height;
}square;

typedef struct text{
	square area;
	cursor_pos rel_pos;
}text;

typedef struct gui{
	square process_area;
	text text;
}gui;

#define COLOR_16
// text and 8 color
enum {RESET = 0, BOLD = 1, DIM = 2, UNDERLINED = 4, BLINK = 5, REVERSE = 7, HIDDEN = 8};
enum {BG_DEFAULT = 49, BG_BLACK = 40, BG_RED = 41, BG_GREEN = 42, BG_YELLOW = 43, BG_BLUE = 44, BG_MAG = 45, BG_CYAN = 46};
enum {FG_DEFAULT = 39, FG_BLACK = 30, FG_RED = 31, FG_GREEN = 32, FG_YELLOW = 33, FG_BLUE = 34, FG_MAG = 35, FG_CYAN = 36};
// select 16 color
enum {BG_DARKGREY = 100};
#ifdef FULLCOLOR
#define FG_DEFAULT 230
#define BG_DEFAULT 0
//TODO change color defaults
typedef struct TerminalState{
	unsigned char text;
	unsigned char fgcolor256;
	unsigned char bgcolor256;
}TerminalState;
void ChangeTerminalState(TerminalState new_terminal);
#endif

#ifdef COLOR_16
void ChangeTerminalState(int);
#endif

cursor_pos global_pos;
gui* runningGUI;
gui* activeGUI;

extern sendmsg(char*,unsigned int,unsigned int);

void ChangeProcessGUI( gui* gui);
void InitProcessGUI( gui* newgui,  square processspace, char* process_name);
void OSPrint(char* str);
void PrintCoord(cursor_pos print_pos, char* str);
void ResetTerminalState();
void ClearScreen();

#endif /* GUI_H_ */
