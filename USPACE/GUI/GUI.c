/*
 * gui.c
 *
 *  Created on: 2014-11-13
 *      Author: William
 *
 *      this process operates in user space. it manages printing to the screen for various processes.
 *
 *      the map will write where it wants...
 *      the console can manage its own mess...
 *
 *      no gui???
 *
 *      I will do a gui since I did one before and it will be done very similarly but 3 main changes
 *
 *      1. a user space process will be doing all the handling, not kspace
 *      2. not all processes will be given a gui. only requesting processes will be given a gui.
 *      3. requesting processes store their own guis. They pass the gui when printing to it. The passed gui is then set as 'active' this is mostly for code reuse.
 */
#include "gui.h"
#include "../Message/trainmessageports.h"
#include "../../MOS/kernel/message.h"

void ChangeTerminalState(int text_code);
void ScreenMessage(char * str, unsigned int size){
	msgReturnCode rtn;
	int i;
	rtn = sendmsg(str, DISPLAY, (unsigned int) size);
	if(rtn != MSG_SUCCESS) {
		// uhhh....
		i = 0;
		// wait a while
		while(i != 10000) i++;
		// try again now??
		rtn = sendmsg(str, DISPLAY, (unsigned int) size);
	}
}

#define BUFFER_MAX_SZ 40
char printbuffer[BUFFER_MAX_SZ+1];
unsigned buffer_index = 0;
void dumpbuffer(){
	// extra free slot to store character in
	if(buffer_index != 0){
		printbuffer[buffer_index] = '\0';
		ScreenMessage(printbuffer, buffer_index);
		buffer_index = 0;
	}
}

void ResetTerminalState();
/* returns true if inside active processes textspace */
bool InsideText(unsigned int rel_x, unsigned int rel_y){
	if(runningGUI->text.area.height > rel_y && runningGUI->text.area.width > rel_x){
		return true;
	} else {
		return false;
	}
}

#define CURSOR_COORD_STRLEN 9
void DrawLine(unsigned int, unsigned int, unsigned int, unsigned int, char*, bool);
char movecursor_str[] = {(char)__ESC,'[','0','0',';','0','0','H', '\0'};
void MoveCursorToCoord(unsigned int x, unsigned int y, bool globalmove){
	/* INPUT: x, y coordinates, moves process cursor and global cursor if valid.
	 * IF globalmove = false, coordinates are assumed to be relative to the topleft corner of the process */
	/* make sure the relative coordinates are inside the area or that these are global coordinates */
	int i;
	if(InsideText(x,y) || globalmove){
		/* OUTPUT a character string the corresponds to coordinates */
			if(!globalmove){
				/* if a relative move, we want to adjust the cursor to moving inside the process space */
				runningGUI->text.rel_pos.x = x;
				runningGUI->text.rel_pos.y = y;
				x += runningGUI->text.area.left;
				y += runningGUI->text.area.top;
			}
			global_pos.x = x;
			global_pos.y = y;

			/* x should be below 80 */
			movecursor_str[2] = (char) y/10 + '0';
			movecursor_str[3] = (char) y%10 + '0';
			/* y should be below 24 */
			movecursor_str[5] = (char) x/10 + '0';
			movecursor_str[6] = (char) x%10 + '0';

			if(CURSOR_COORD_STRLEN < BUFFER_MAX_SZ - buffer_index){
				for(i = 0; i < CURSOR_COORD_STRLEN; i++){
					printbuffer[buffer_index++] = movecursor_str[i];
					dumpbuffer();
				}
			} else {
				dumpbuffer();
				ScreenMessage(movecursor_str,sizeof(char)*9);
			}
	}
}

/* returns true if inside active processes space */
bool InsideProcess(unsigned int rel_x, unsigned int rel_y){
	if(runningGUI->process_area.height > rel_y && runningGUI->process_area.width > rel_x){
		return true;
	} else {
		return false;
	}
}

void EraseActiveLine(unsigned int line){
/* erase the given line on the passed process */
	DrawLine(0, 						// x of top corner
		line, 							// y of top corner
		runningGUI->text.area.width,	// x of far corner
		line, 							// y of far corner
		" \0",							// symbol
		LOCAL);							// relative to the process coordinates
	MoveCursorToCoord(0,				// restore to the previous line
		line,
		LOCAL);
}

void OSPrint(char* str){
	static unsigned char str_ignore = 0;
	int i = 0;
	while(str[i] != STREND){
		if(str_ignore == 0){
		if(runningGUI->text.rel_pos.x == 0) {EraseActiveLine(runningGUI->text.rel_pos.y);}
		switch(str[i])
		{
		case __RETURNCHR:
			/* translate the cursor position to the beginning of the box, next line
			 * if at the end of height, go to the top of the window again.  */
			MoveCursorToCoord(0,
					(runningGUI->text.rel_pos.y + 1) % (runningGUI->text.area.height - 1),
					LOCAL);
			//EraseActiveLine(runningGUI->text.rel_pos.y);
			/* LINESTR indicates to write_out that this is a command. */
		break;
		case __BS:
			/* if in top left, do nothing.
			 * if in left, not top, move up one and to far right and write backspace
			 * otherwise, do normal */
			if(runningGUI->text.rel_pos.x == 0 && runningGUI->text.rel_pos.y == 0){
				/* do nothing */
			} else if (runningGUI->text.rel_pos.x == 0){
				MoveCursorToCoord(runningGUI->text.area.width - 1, runningGUI->text.rel_pos.y - 1, LOCAL);
				printbuffer[buffer_index++] = __CHR_SPACE;
				MoveCursorToCoord(runningGUI->text.rel_pos.x - 1, runningGUI->text.rel_pos.y, LOCAL);
			} else {
				MoveCursorToCoord(runningGUI->text.rel_pos.x - 1, runningGUI->text.rel_pos.y, LOCAL);
				printbuffer[buffer_index++] = __CHR_SPACE;
				MoveCursorToCoord(runningGUI->text.rel_pos.x, runningGUI->text.rel_pos.y, LOCAL);
			}
		break;
		case __BACKSLASH:
			/* if next character is an 'n', print new line and ignore the backslash */
			if(str[i+1] == 'n') {
				/* translate the cursor position to the beginning of the box, next line
				 * if at the end of height, go to the top of the window again.  */
				MoveCursorToCoord(0,
						(runningGUI->text.rel_pos.y + 1) % (runningGUI->text.area.height - 1),
						LOCAL);
				//EraseActiveLine(runningGUI->text.rel_pos.y);
				i++;
			} else {
				/* if not an n, print out the backslash as normal. */
				printbuffer[buffer_index++] = __INS_BACKSLASH;
				runningGUI->text.rel_pos.x++;
				global_pos.x++;
				if(!(runningGUI->text.rel_pos.x < runningGUI->text.area.width)){
					MoveCursorToCoord(0,
						(runningGUI->text.rel_pos.y + 1) % (runningGUI->text.area.height - 1), LOCAL);
					//EraseActiveLine(runningGUI->text.rel_pos.y);
				}
			}
		break;
		case __ESC:
			/* ignore the next two characters, whenever they are received */
			str_ignore = 2; // block weird escape attempts via print
		break;
		default:
			/* write out the character.
			 * if at the end of the width, go to the next line
			 * if at the end of the width, height, go to the top of the window again. */
			printbuffer[buffer_index++] = str[i];
			runningGUI->text.rel_pos.x++;
			global_pos.x++;
			if(!(runningGUI->text.rel_pos.x < runningGUI->text.area.width)){
				MoveCursorToCoord(0,
					(runningGUI->text.rel_pos.y + 1) % (runningGUI->text.area.height - 1), LOCAL);
				EraseActiveLine(runningGUI->text.rel_pos.y);
			}
		break;
		}
		} else {str_ignore--;}
		i++;
		if(buffer_index == BUFFER_MAX_SZ){
			dumpbuffer();
		}
	}
	dumpbuffer();
}

void PrintCoord(cursor_pos print_pos, char* str){
// wrapper for other functions
	if(runningGUI->text.rel_pos.x != print_pos.x || runningGUI->text.rel_pos.y != print_pos.y){
	MoveCursorToCoord(print_pos.x,print_pos.y,LOCAL);
	}
	OSPrint(str);
}

void DrawLine(unsigned int x1, unsigned int y1, unsigned int x2, unsigned int y2, char* symbol, bool isGlobalMode){
	cursor_pos holdGUIPos;
	unsigned int temp, cx, cy;
	float diffx, diffy, dx, dy, max_div;

	if(InsideProcess(x1,y1) && InsideProcess(x2,y2) || isGlobalMode){
	/* store the current position of the cursor */
		holdGUIPos = runningGUI->text.rel_pos;

		/* x1 is always smaller than x2 to make this easier */
		if(x1 > x2){
			temp = x2;
			x2 = x1;
			x1 = temp;
		}
		/* y1 is always smaller than y2 to make this easier */
		if(y1 > y2){
			temp = y2;
			y2 = y1;
			y1 = temp;
		}
		/* x1 < x2, y1 < y2 */
		MoveCursorToCoord(x1, y1, isGlobalMode);
		/* find the difference between x and y, divide the smaller by the bigger,
		 * and move in that direction until either x or y is bigger */
		diffx = x2 - x1;
		diffy = y2 - y1;

		/* use whichever is bigger as 1 when moving */
		max_div = (diffx > diffy) ? diffx : diffy;
		dx = diffx/max_div;
		dy = diffy/max_div;

		/* set up current x and current y initial values */
		cx = x1;
		cy = y1;
		int i = 0;
		while(cx + i*dx < x2 || cy + i*dy < y2) {
			if(y1 != y2) {
				MoveCursorToCoord(cx + (unsigned int)i*dx, cy + (unsigned int)i*dy, isGlobalMode);
			}
			ScreenMessage(symbol,1);
			i++;
		} /* written out line to the screen */
		/* restore the original state */
		MoveCursorToCoord(holdGUIPos.x,holdGUIPos.y, isGlobalMode);
	} else {
		ScreenMessage("Invalid Line attempt",20);
	}
}

void ChangeProcessGUI(gui* gui){
	/* move the global cursor to somewhere inside the process depending on where it was before */
	if(runningGUI != gui){
		MoveCursorToCoord(
				(unsigned int) (gui->text.area.left + gui->text.rel_pos.x),
				(unsigned int) (gui->text.area.top + gui->text.rel_pos.y),
				GLOBAL);

		/* set the process as active */
		runningGUI = gui;
		/* this is now the active process, cursor is in correct position */
	}
}

/* uses draw line to draw a square */
void DrawSquare(square square, char* symbol){
	/* Uses 'drawline' to draw a square */
	/* draw top horizontal line */
	DrawLine(square.left,	square.top,		square.left + square.width,	square.top, 	symbol, 	GLOBAL);
	/* draw left vertical line */
	DrawLine(square.left,	square.top,		square.left,	square.top + square.height - 2, 	symbol, 	GLOBAL);
	/* draw right vertical line */
	DrawLine(square.left + square.width - 1,	square.top + square.height - 2,	square.left + square.width - 1,	square.top,	symbol, 	GLOBAL);
	/* draw bottom horizontal line */
	DrawLine(square.left, 	square.top + square.height - 2,	square.left + square.width,	square.top + square.height - 2, symbol,	GLOBAL);
}

#define BORDER_COLOR 17
#define PROC_SYM " \0"
void DrawProcess(gui* gui, char* name){
	/* draw a the passed process space. The passed process has already had its spatial characteristics set */
	square holdarea;
	unsigned int str_cnt = 0;
	holdarea = gui->process_area;

	ChangeTerminalState(BG_GREEN);

	DrawSquare(holdarea, PROC_SYM);
	DrawLine(holdarea.left,
			gui->text.area.top - 1,
			holdarea.left + gui->text.area.width + 1,
			gui->text.area.top - 1, PROC_SYM, 	GLOBAL);

	ResetTerminalState();

	MoveCursorToCoord(gui->process_area.left + 1, gui->process_area.top + 1, GLOBAL);
	while(*(name+str_cnt) != '\0') str_cnt++;
	ScreenMessage(name, str_cnt);

	MoveCursorToCoord(gui->text.area.left, gui->text.area.top, GLOBAL);

	gui->text.rel_pos.x = 0;
	gui->text.rel_pos.y = 0;
}

#define drawarea
void InitProcessGUI(gui* newgui, square processspace, char* process_name){
	/* passes the space the process will get, then draws out. modifies an empty gui */
	newgui->process_area = processspace;
	newgui->text.area.top = processspace.top + 3;
	newgui->text.area.left = processspace.left + 1;
	newgui->text.area.width = processspace.width - 2;
	newgui->text.area.height = processspace.height - 4;
	/* Text area derived from passed process space. */

	DrawProcess(newgui, process_name);
}

void EraseProcess(gui* gui){
/* draw out lines of spaces to erase the current process */
	int k;
	for(k = 0; k < gui->process_area.height; k++){
		DrawLine(gui->process_area.left,
				gui->process_area.top + k,
				gui->process_area.left + gui->process_area.width - 1,
				gui->process_area.top,
				" ",	GLOBAL);
	}
}

/***************************************************
 *                 CONTROL GRAPHICS
 ***************************************************/

#ifdef FULL_COLOR
char TermStr[] = {__ESC, '[',	// 0,1			| BEGIN
		'0','0',';', 			// 2,3,4 		| TEXT SET
		'3','8',';','5',';',	// 5,6,7,8,9	| TEXT COLOR 256
		'0','0','0',';',		// 10,11,12,13	| TEXT COLOR GOES HERE
		'4','8',';','5',';',	// 14,15,16,17	| BG COLOR 256
		'0','0','0',';',		// 18,19,20,21	| BG COLOR GOES HERE
		'm','\0'};				// 22, 23		| END
void ChangeTerminalState(TerminalState new_terminal){
/* Adjusts the properties of the VT100 terminal window with escape sequences */
	TermStr[2] = new_terminal.text/10 + '0';
	TermStr[3] = new_terminal.text%10 + '0';

	TermStr[10] = new_terminal.fgcolor256/100 + '0';
	TermStr[11] = new_terminal.fgcolor256/10 + '0';
	TermStr[12] = new_terminal.fgcolor256%10 + '0';

	TermStr[18] = new_terminal.bgcolor256/100 + '0';
	TermStr[19] = new_terminal.bgcolor256/10 + '0';
	TermStr[20] = new_terminal.bgcolor256%10 + '0';
	writeString(TermStr);
}
#endif

#ifndef FULL_COLOR
char TermStr[] = {__ESC, '[', '0','0','m','\0'};
void ChangeTerminalState(int text_code){
/* adjusts how future text is displayed */
	TermStr[2] = text_code/10 + '0';
	TermStr[3] = text_code%10 + '0';

	ScreenMessage(TermStr, 6);
}
#endif

void ResetTerminalState(){
/* clears terminal properties */
#ifdef FULL_COLOR
	TerminalState reset_terminal;
	reset_terminal.text = RESET;


	reset_terminal.fgcolor256 = FG_DEFAULT;
	reset_terminal.bgcolor256 = BG_DEFAULT;
#endif

	ChangeTerminalState(RESET);
}

#define CLEAR_STR "\033[2J\033[;H\0"
#define CLEAR_STR_SZ 9
void ClearScreen(){
	ScreenMessage(CLEAR_STR, CLEAR_STR_SZ);
}
