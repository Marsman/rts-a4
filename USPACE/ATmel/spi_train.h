/*
 * spi_train.h
 *
 *  Created on: 2014-11-14
 *      Author: William
 */

#ifndef SPI_TRAIN_H_
#define SPI_TRAIN_H_

#define NULL 0

#define ATMEL1 0x01
#define ATMEL2 0x02
#define ATMEL3 0x03

#define ATMEL3_FIRSTHALLSENSOR 9
#define ATMEL_MAXSPEED 7
#define XMIT_STR_LEN 6

#define HALLSENSOR_RESETCMD 0xC0
#define SEG_VELOCITYALLCMD 0xbe
#define HALLSENSOR_POLLCMD 0xA0, 0xff, 0xff
#define HALLSENSOR_ALLRESETCMD 0xce, 0xff, 0xff
#define HALLSENSOR_QUEUERESETCMD 0xae, 0xff, 0xff
#define SEG_VELOCITYCMD 0xb0
#define SWITCH_CHANGECMD 0xd0
#define NOARG 0xff

#define ATMEL_TRACK_TURNED 0
#define ATMEL_TRACK_STRAIGHT 1

#define _CCW 0
#define _CLW 1

#define HALLQNTY 32
#define SWITCHQNTY 9
#define SEGQNTY 16

#define _SEGMENT_OFF 0

void ResetAllHallSensors();
void ResetHallSensor(unsigned char hallinx);
unsigned char PollHallSensor(unsigned atmel);
void pon(unsigned char, char);
void poff(unsigned char);
void poffall();
void SwitchToState(unsigned char switchid, char state);
void ResetSwitches();
void SegmentVelocity(unsigned char, char);
void ResetHallSensorQueue();

#endif /* SPI_TRAIN_H_ */
