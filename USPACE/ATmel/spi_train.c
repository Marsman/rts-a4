/*
 * spi_train.c
 *
 *  Created on: 2014-11-14
 *      Author: William
 *
 *      Collection of functions for communciating with the trainset
 *
CORTEX -> ATMEL message passing
AA		CMD			ARG1		ARG2		55
START	COMMAND		ARG1		ARG2		END

ATMEL -> CORTEX message passing
CC        	CMD			RES1		RES2		99/3f
START		COMMAND    	RES1		RES2		END

 */

#include "spi.h"
#include "spi_train.h"

unsigned char ATMEL_XMIT_STR[] = {0xaa, 0x00, 0x00, 0x00, 0x55, 0xff};
/* start, cmd, res1, res2, end, ff */
unsigned char ATMEL_RECV_STR[] = {0xff, 0xcc, 0x00, 0x00, 0x00, 0x99};
/* ff, start, cmd, res1, res2, success/fail} */
#define ATMEL_CMD  1
#define ATMEL_ARG1 2
#define ATMEL_ARG2 3
#define ATMEL_RECV_ADJ 1

typedef struct atmel_rtn {
	unsigned char arg1;
	unsigned char arg2;
}atmel_res;

atmel_res TransmitInstruction(unsigned atmel_number, unsigned char cmd,
		unsigned char arg1, unsigned char arg2){
/* Transmit an instruction. Returns RES1 and RES2 via a passed array.

CORTEX -> ATMEL message passing
AA		CMD			ARG1		ARG2		55
START	COMMAND		ARG1		ARG2		END

ATMEL -> CORTEX message passing
CC        	CMD			RES1		RES2		99/3f
START		COMMAND    	RES1		RES2		END
 * */
	int i = 0;

	// Assign CMD, arg1, arg2 to the string
	ATMEL_XMIT_STR[ATMEL_CMD] = cmd;
	ATMEL_XMIT_STR[ATMEL_ARG1]= arg1;
	ATMEL_XMIT_STR[ATMEL_ARG2]= arg2;

	for(;i < XMIT_STR_LEN; i++){
	// transmit the XMIT string and store the response in RECV
		SPIDataTransmit(ATMEL_XMIT_STR[i], atmel_number);
		ATMEL_RECV_STR[i] = SPIDataReceive();
	}

	atmel_res result;

	result.arg1 = ATMEL_RECV_STR[ATMEL_RECV_ADJ + ATMEL_ARG1];
	result.arg2 = ATMEL_RECV_STR[ATMEL_RECV_ADJ + ATMEL_ARG2];
	/* return arguments */

	return result;
}

void ResetHallSensorQueue(){
/* Resets queue for hall sensors; called initially
CMD: CE
A1: FF
A2: FF
 */
TransmitInstruction(ATMEL1, HALLSENSOR_QUEUERESETCMD);
TransmitInstruction(ATMEL3, HALLSENSOR_QUEUERESETCMD);
/* reset all on Atmel 1 */
/* reset all on Atmel 3 */
}

void ResetAllHallSensors(){
/* Resets all hall sensors; called initially

CMD: CE
A1: FF
A2: FF
 */
/* reset all on Atmel 1 */
TransmitInstruction(ATMEL1, HALLSENSOR_ALLRESETCMD);
/* no need for return arguments */
}

void ResetHallSensor(unsigned char hallinx){
/* Resets a specific hall sensor. Recall: 1-8 on Atmel 1, 9-32 on Atmel 3

CMD: C0
ARG1: HS#
ARG2: FF
 */
TransmitInstruction(ATMEL1, HALLSENSOR_RESETCMD, hallinx, NOARG);
}

unsigned char PollHallSensor(unsigned atmel){
/* return the value of the inspected hall sensor. Should be atmel 1 or 3.
CMD: A0
ARG1: FF
ARG2: FF

RESP
CMD: A0
R1 : HS# <- which triggered = arg1
R2: FF
 * */
unsigned char triggered_hall;
if(atmel != ATMEL2) {
	/* ensure not sending atmel 2 an undefined command */
	triggered_hall = TransmitInstruction(atmel, HALLSENSOR_POLLCMD).arg1;
	ResetHallSensor(triggered_hall);
	return triggered_hall;
}
	return NULL;
}
/*
CMD: A0
ARG1: FF
ARG2: FF

RESP
CMD: A0
R1 : HS# <- which triggered
R2: FF
 */


void AllSegmentsVelocity(char v){
/* Initialize segment speed and direction for all segments. Uses ATmel 1
 * -ve = backwards, +ve = forwards. -7 .. 7. last bit is sign, first seven are number.
velocity_byte = [dir][-][-][-][-][s2][s1][s0]
M & D for all tracks
CMD: BE
ARG1: FF
ARG2: M & D
 * */
	/* no need to adjust this char, it is what the atmel is expecting */
	TransmitInstruction(ATMEL1, SEG_VELOCITYALLCMD, NOARG, v);
}

//#define PORTF_TEST
void poffall(){
/* wrapper for all segments velocity */
	AllSegmentsVelocity(_SEGMENT_OFF);
#ifdef PORTF_TEST
	AllSegmentsVelocity(5);
	SegmentVelocity(5,0);
#endif
}

void SegmentVelocity(unsigned char segid, char v){
/* Switch a single track segment. Track segments range from 1 to 16

-ve value = Counter clockwise
+ve value = clockwise

M & D for a single track segment
CMD: B0
ARG1: SEG #
ARG2: M & D
 */
/* check and ensure this is a valid velocity.
 * might not make a difference, but invalid speed entries have undefined behaviour */
	/* no need to adjust v, it is what the atmel is expecting */
	TransmitInstruction(ATMEL1, SEG_VELOCITYCMD, segid, v);
}

void pon(unsigned char segid, char v){
	/* wrapper function for external use */
	SegmentVelocity(segid, v);
}

void poff(unsigned char segid){
	/* wrapper function for external use */
	SegmentVelocity(segid, _SEGMENT_OFF);
}
extern unsigned char switch_state[];
void SwitchToState(unsigned char switchid, char state){
/* switch id from 1 - 9, state can be 0 [turned] or 1 [straight]

AA | DO | S# | DIR | 55
S# must be 1 .. 9
DIR is a byte, requires 0 - TURN, 1 - STRAIGHT
 */

if(state == ATMEL_TRACK_TURNED || state == ATMEL_TRACK_STRAIGHT){
	/* ensure valid direction being sent */
	if(switchid==6||switchid==8)//switch both 6 and 8 to get onto or off
		//inner track
	{
		TransmitInstruction(ATMEL2, SWITCH_CHANGECMD, 6, state);
		TransmitInstruction(ATMEL2, SWITCH_CHANGECMD, 8, state);
	}
	else if(switchid==7||switchid==3)
	{
		TransmitInstruction(ATMEL2, SWITCH_CHANGECMD, 3, state);
		TransmitInstruction(ATMEL2, SWITCH_CHANGECMD, 7, state);
	}
	else
	TransmitInstruction(ATMEL2, SWITCH_CHANGECMD, switchid, state);
}
switch_state[switchid]=state;

}

void ResetSwitches(){
/* resets all of the switches to pointing straight ahead */
unsigned char i;
for(i=1; i <= SWITCHQNTY;i++){
	SwitchToState(i, ATMEL_TRACK_STRAIGHT);
}
}
